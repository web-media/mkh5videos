#!/usr/bin/env python3.7
""" mkH5videos was created by Austin Byron dev@3ch0peak.com for personal use 
and has released the source code under the GPL license for others to enjoy.
Please feel free to modify redistribute at your convenience but I do hope that
my work herein always remains free of charge. It is without warranty and I am
not responsible for damages should any occur.

This script creates HTML5 encoded videos in webm and H264(mp4) formats in
multiple sizes.

This file depends on the following programs being installed:
    1.) FFmpeg 2.8.6
        Codecs:
            (a): libx264
            (b): libvpx
    2.) FFprobe
    3.) qt-faststart

This program also uses GSAP for javaScript animations. If you are going to be 
using this player commercially you need to check out their terms of service.
http://greensock.com/licensing/

Austin Byron and/or 3ch0peak are in no way affiliated with FFmpeg other than a
profound respect for the hard work they have done to make the best encoders 
for video, audio.

This script does not include FFmpeg. You will have to install and set up on 
your system.

Debian:
  check if available:
    apt-cache search
  if available:
    sudo apt-get install ffmpeg

If you are running Windows, Mac or need other installation instructions:
  https://ffmpeg.org/download.html """

from sys import argv, exit as sExit, stdout as syStdout
from os import (name as oName, mkdir as oMkdir, getcwd as oGetcwd,
                listdir as oListdir, remove as oRemove)
from os.path import (abspath as oAbspath, dirname as oDirname, join as oJoin,
                     exists as oExists, isabs as oIsabs, getsize as oGetsize,
                     basename as oBasename, splitext as oSplitext,
                     isfile as iSfile, isdir as oIsdir) 
import json
from re import findall
from re import compile as rCompile
from psutil import subprocess as subProc, cpu_count
from PIL import Image, UnidentifiedImageError


class mkH5Video():
    """    This class will encode videos for the web in two codecs(.mp4, .webm)
and in 6 different resolutions each, for a total of 12 videos.

Not only does the script encode videos, it also makes all the HTML5, CSS, 
javaScript, JSON and PHP you need to get your player up and running.

The player is very customizable. The button colors can be changed. The program
makes a playlist and there are five different thumbnails and placeholder images
that you can choose from.

 """

    def __init__(self, *arg, **karg):
        """mkH5video usage:

$ python mkH5videos.py [*args] {**karg}

example:

$ python mkH5videos.py myvideo1.mp4 myvideo2.webm allWeb=true

$ python mkH5videos.py myvideo.avi setsize=1920x1080 \\
> bitrate=1900000 maxbit=2100000 buffersize=4200000 json=true

-------------------------------------------------------------------------

Options:
  General:

    -h, --help           Prints this help menu and exit.

    -v, --version        Prints the program version and exit.

    -y                   Forces the answer yes to all questions.

    -n                   Sets FFmepg to default and may need answers before
                           tasks are completed. Handy for testing video size.   
  Advanced:
    html=                Takes true, True or false, False. Defaults to False
                           Makes html5 file.

            Shortcut: -html
                Automatically sets the html variable to True.

    json=                Takes true, True or false, False. Defaults to False.
                           Makes json file to be used by JSfile. The function
                           reads all files from the folders created by this
                           program and makes valid json accordingly. You can 
                           make and remake the json file as needed.

            Shortcut: -json
                Automatically set's the json variable to True.

    jScript=             Takes true, True or false, False. Defaults to False.
                           Makes javaScript file.

            Shortcut: -javascript
                Automatically sets the javascript variable to True.

    php=                 Takes true, True or false, False. Defaults to False.
                           Makes PHP file.

            Shortcut: -php
                Automatically sets the php variable to True.

    css=                 Takes true, True or false, False. Defaults to False.
                           Makes CSS file. **Needs the JSON file**

            Shortcut: -css
                Automatically sets the css variable to True.

    allWeb=               Takes true, True or false, False. Defaults to False
                           Makes html5, css, php and javaScript files.

            Shortcut: -all-web
                Automatically sets the allWeb variable to True.

    playersize=          Takes numbers separated by 'x' : 1920x1080
                           width then height of video player in your HTML.
                           Default is set to 16:9 = 400x240 4:3 = 400x300.

    thumbsize=           Takes numbers separated by 'x' : 320x180
                           width then height of video player in your HTML.
                           Default is set to 16:9 = 120x72 4:3 = 120x90.

    widescreen=          Takes true, True or false, False.

    threads=             Takes number. The number of threads or CPU to use.
                           Defaults to all but 1 of your CPU's.

    setsize=             Takes numbers separated by 'x' : 1920x1080
                           width then height of video. If set the following
                           options are required.

    bitrate=             Takes number. The bitrate of the video. Without
                           setsize this will be ignored.

    maxbit=              Takes number. The maximum bitrate for the video.
                           Without setsize this will be ignored.

    buffersize=          Takes number. The buffersize for the video.
                           Without setsize this will be ignored.

    rez_limiter=         Takes a list of resolutions to be encoded in the range
                           of ['xd', 'hd', 'hq', 'sd', 'm'] qualities. Largest 
                           from left to right. 

                           **Note:
                              if passing arguments in the terminal remember that
                              you  cannot use spaces in your list.

                              Example:
                                rez_limiter=['hd','hq','sd']

 """
        self.baseDir = oAbspath(oDirname(__file__))

        self.vidSetrs = MutableDict([('make_videos', True),
                                     ('ask_confirmation', True),
                                     ('ffQuestions', False)])

        self.vidSetrs['vDatas'] = {}

        ## Let's do "Get Help!!"
        if 1 in [1 for x in ['-h', '-help'] if x in arg]:
            print(self.mkHelp())
            sExit()

        ## Show mkH5vodeo3 version information.
        if 1 in [1 for x in ['-v', '-version'] if x in arg]:
            print('0.5')
            sExit()


        ## ______ Set up the *args ______ ##

        self.vidSetrs['ask_confirmation'] = True
        if 1 in [1 for x in ['-y', '-yes'] if x in arg]:
            ## this flag is for mkH5video specific confirmation questions 
            ## in this program. 

            self.vidSetrs['ask_confirmation'] = False            


        self.vidSetrs['ffQuestions'] = False
        if 1 in [1 for x in ['-n', '-no_questions'] if x in arg]:
            ## this flag is for ffmpeg specific questions. The default 
            ## behavior it to not ask for confirmation.

            self.vidSetrs['ffQuestions'] = True            


        if arg != ():
            self.vidSetrs['vDatas'] = parse_video_info(arg)
            if 'make_videos' in karg.keys() and karg['make_videos'] is False:
                self.vidSetrs['make_videos'] = False
            else:
                self.vidSetrs['make_videos'] = True

        else:
            self.vidSetrs['make_videos'] = False



        ## ______ Set up the *kargs ______ ##

        self.vidSetrs = set_widescreen(self.vidSetrs, karg)

        self.vidSetrs = set_size(self.vidSetrs, karg)
  
        self.vidSetrs = set_cores(self.vidSetrs, karg)
  
        self.vidSetrs = set_player_size(self.vidSetrs, karg)
  
        self.vidSetrs = set_thumb_size(self.vidSetrs, karg)
  
        self.vidSetrs = set_json(self.vidSetrs, arg, karg)
  
        self.vidSetrs = set_html(self.vidSetrs, arg, karg)
  
        self.vidSetrs = set_javascript(self.vidSetrs, arg, karg)
  
        self.vidSetrs = get_javascript(self.vidSetrs, karg)
  
        self.vidSetrs = set_css(self.vidSetrs, arg, karg)
  
        self.vidSetrs = set_php(self.vidSetrs, arg, karg)
  
        self.vidSetrs = set_all_web(self.vidSetrs, arg, karg)
  

        if self.vidSetrs['make_videos']:
            self.encode()

        self.web_builder()

    def encode(self):
        """ This method does the encoding and video plaiceholder image creation. """
        
        self.mkDir()
        for fName in self.vidSetrs['vDatas']:
            ## make all the directories needed for videos, html and thumbnails
            nPath = oJoin(self.baseDir,
                          'mkH5videos',
                          'webVideos',
                          fix_filename(self.vidSetrs['vDatas'][fName]['name']))

            webmPath, mp4Path, imgPath, thumbPath = self.mkDir(setup=nPath)

            tOffset = 4
            for x, fSize in enumerate(self.vidSetrs['sizs']):
                if int(self.vidSetrs['vDatas'][fName]['bitRate']) >= fSize['bit_rate']:
                    ## ---->  Commands to execute <---- ##
                    
                    ## create the ffmpeg commands for mp4

                    temp_loc = oJoin(self.baseDir,
                                     'mkH5videos',
                                     'webVideos',
                                     fix_filename(fName),
                                     'mp4',
                                     'temp.mp4')

                    mp4outFile = f"""{oJoin(self.baseDir,
                                            'mkH5videos',
                                            'webVideos',
                                            fix_filename(fName),
                                            'mp4',
                                            fix_filename(fName))}-{str(fSize['width']) +
                                            'x' + str(fSize['height'])}"""

                    webMoutFile = f"""{oJoin(self.baseDir,
                                             'mkH5videos',
                                             'webVideos',
                                             fix_filename(fName),
                                             'webm',
                                             fix_filename(fName))}-{str(fSize['width']) +
                                             'x' + str(fSize['height'])}"""

                    # I removed this because I wanted to see it it was necessary
                    # '-crf', '23',
                    mp4Pass1 = ['ffmpeg',
                                '-i', f"{self.vidSetrs['vDatas'][fName]['loc']}",
                                '-vcodec', 'libx264',
                                '-vprofile', 'high',
                                '-preset', 'slow',
                                '-b:v', f"{fSize['bit_rate']}",
                                '-maxrate', f"{fSize['max_bit']}",
                                '-bufsize', f"{fSize['buffer_size']}",
                                '-vf',
                                f"scale={fSize['width']}:{fSize['height']}",
                                '-threads', f"{self.vidSetrs['cores']}",
                                '-pass', '1',
                                '-an',
                                '-pix_fmt', 'yuv420p',
                                '-f', 'mp4',
                                f"{set_dev_null()}",
                                '-y']

                    mp4Pass2 = ['ffmpeg',
                                '-i', f"{self.vidSetrs['vDatas'][fName]['loc']}",
                                '-vcodec', 'libx264',
                                '-vprofile', 'high',
                                '-preset', 'slow',
                                '-b:v', f"{fSize['bit_rate']}",
                                '-maxrate', f"{fSize['max_bit']}",
                                '-bufsize', f"{fSize['buffer_size']}",
                                '-vf',
                                f"scale={fSize['width']}:{fSize['height']}",
                                '-threads', f"{self.vidSetrs['cores']}",
                                '-pass', '2',
                                '-strict', '-2',
                                '-pix_fmt', 'yuv420p',
                                '-f', 'mp4',
                                f'{temp_loc}',
                                '-y']

                    fastStart = ['qt-faststart', f'{temp_loc}', f'{mp4outFile}.mp4']

                    webmPass1 = ['ffmpeg',
                                 '-i', f"{self.vidSetrs['vDatas'][fName]['loc']}",
                                 '-codec:v', 'libvpx',
                                 '-quality', 'good',
                                 '-cpu-used', '0',
                                 '-b:v', f"{fSize['bit_rate']}",
                                 '-qmin', '10',
                                 '-qmax', '42',
                                 '-maxrate', f"{fSize['max_bit']}",
                                 '-bufsize', f"{fSize['buffer_size']}",
                                 '-threads', f"{self.vidSetrs['cores']}",
                                 '-vf',
                                 f"scale={fSize['width']}:{fSize['height']}",
                                 '-an',
                                 '-pass', '1',
                                 '-f', 'webm',
                                 f'{set_dev_null()}',
                                 '-y']

                    webmPass2 = ['ffmpeg',
                                 '-i', f"{self.vidSetrs['vDatas'][fName]['loc']}",
                                 '-codec:v', 'libvpx',
                                 '-quality', 'good',
                                 '-cpu-used', '0',
                                 '-b:v', f"{fSize['bit_rate']}",
                                 '-qmin', '10',
                                 '-qmax', '42',
                                 '-maxrate', f"{fSize['max_bit']}",
                                 '-bufsize', f"{fSize['buffer_size']}",
                                 '-threads', f"{self.vidSetrs['cores']}",
                                 '-vf',
                                 f"scale={fSize['width']}:{fSize['height']}",
                                 '-codec:a', 'libvorbis',
                                 '-b:a', '128k',
                                 '-pass', '2',
                                 '-f', 'webm',
                                 f'{webMoutFile}.webm',
                                 '-y']

                    thumb = ['ffmpeg',
                             '-itsoffset', f'-{tOffset}',
                             '-i', f'{mp4outFile}.mp4',
                             '-vcodec', 'png',
                             '-vframes', '1',
                             '-an',
                             '-f', 'rawvideo',
                             '-s', f"{self.vidSetrs['thumbSize'][0]}x{self.vidSetrs['thumbSize'][1]}",
                             '-y',
                             f"{oJoin(thumbPath, fix_filename(self.vidSetrs['vDatas'][fName]['name']))}-{x}.png"]

                    plcPic = ['ffmpeg',
                              '-itsoffset', f'-{tOffset}',
                              '-i', f'{mp4outFile}.mp4',
                              '-vcodec', 'png',
                              '-vframes', '1',
                              '-an',
                              '-f', 'rawvideo',
                              '-s', f"{self.vidSetrs['playerSize'][0]}x{self.vidSetrs['playerSize'][1]}",
                              '-y',
                              f"{oJoin(imgPath, fix_filename(self.vidSetrs['vDatas'][fName]['name']))}-{x}.png"]

                    if not self.vidSetrs['vDatas'][fName]['isMov']:
                        mp4Pass1.remove('-pix_fmt')
                        mp4Pass1.remove('yuv420p')
                        mp4Pass2.remove('-pix_fmt')
                        mp4Pass2.remove('yuv420p')

                    if self.vidSetrs['ffQuestions'] is True:
                        mp4Pass1.remove('-y')
                        mp4Pass2.remove('-y')
                        webmPass1.remove('-y')
                        webmPass2.remove('-y')
                        thumb.remove('-y')
                        plcPic.remove('-y')

                    encPass = MutableDict({'Encoding MP4 first pass.': mp4Pass1,
                                           'Encoding MP4 second pass.': mp4Pass2,
                                           'Moving meta-data to beginning.': fastStart,
                                           'Taking a plaice holder picture.': plcPic,
                                           'Taking a thumbnail picture.': thumb,
                                           # 'Removing Temporary MP4 files.': rmTemp,
                                           'Encoding WEBM first pass.': webmPass1,
                                           'Encoding WEBM second pass.': webmPass2})

                    if oName == 'nt':
                        # self.rmLog = ['del', '%s*log*'%self.baseDir]
                        rmTemp = ['del', f'{temp_loc}']

                        encPass.update({'Remove movie temporary files.': rmTemp})

                    else:
                        rmLog = ['rm', f'{self.baseDir}*.log']
                        rmTemp = ['rm', f'{temp_loc}']

                        encPass.insert_before('Encoding WEBM first pass.',
                                              'Remove movie temporary files.',
                                              rmTemp)
                        encPass.update({'Remove movie log files.': rmLog})
                        # encPass.update({'Remove movie temporary files.': rmTemp})

                    print(f'\n Starting encode for:\n>>  {nPath}\n')
                    for key, ePass in encPass.items():
                        print(f"{key}\n  command: {' '.join(ePass)}\n")
                        process = subProc.Popen(ePass, stdout=subProc.PIPE)
                        output = process.communicate()[0]
                        print(output)

                tOffset = tOffset + 15
    

    def web_builder(self):
        """ This method will build some or all of the parts needed to build this
        players functionality.
          1. HTML
          2. Javascript
          3. CSS
          4. JSON
          5. PHP"""

        if self.vidSetrs['allWeb']:
            mkJson(self.baseDir)
            mkHTML(self.baseDir, self.vidSetrs['playerSize'])
            mkJS(self.baseDir, False)
            mkPHP(self.baseDir)
            mkCSS(self.baseDir)
        else:

            if self.vidSetrs['json']:
                mkJson(self.baseDir)

            if self.vidSetrs['html']:
                mkHTML(self.baseDir, self.vidSetrs['playerSize'])

            if self.vidSetrs['jScript']:
                mkJS(self.baseDir, False)

            if self.vidSetrs['getJS']:
                mkJS(self.baseDir, True)

            if self.vidSetrs['php']:
                mkPHP(self.baseDir)

            if self.vidSetrs['css']:
                mkCSS(self.baseDir)


    def mkDir(self, setup=''):
        '''This method builds the necessary directories the player needs videos
        and their content.'''

        if setup == '':
            h5Vid = oJoin(self.baseDir, 'mkH5videos')
            if not oExists(h5Vid):
                oMkdir(h5Vid)

            if not oExists(oJoin(h5Vid, 'webVideos')):
                webVideos = oJoin(h5Vid, 'webVideos')
                oMkdir(webVideos)

            if not oExists(oJoin(h5Vid, 'scripts')):
                scrip = oJoin(h5Vid, 'scripts')
                oMkdir(scrip)
            return ()
        else:
            nPath = setup
            if not oExists(nPath):
                oMkdir(nPath)

            webmPath = oJoin(nPath, 'webm')
            if not oExists(webmPath):
                oMkdir(webmPath)

            mp4Path = oJoin(nPath, 'mp4')
            if not oExists(mp4Path):
                oMkdir(mp4Path)

            imgPath = oJoin(nPath, 'images')
            if not oExists(imgPath):
                oMkdir(imgPath)

            thumbPath = oJoin(imgPath, 'thumb')
            if not oExists(thumbPath):
                oMkdir(thumbPath)

            return (webmPath, mp4Path, imgPath, thumbPath)

    def mkHelp(self):
        """ Let's do "Get help!" """
        return f'{__doc__}\n{self.__init__.__doc__}\n{_helpNotes}'


def fix_filename(fName, switch=False):
    """ This will fix the spaces and unwanted chars in the filenames. """
    if not switch:
        return fName.replace(' ', '_').replace('(', '').replace(')', '').replace("'", '')
    else:
        return fName.replace('_', ' ')


def set_widescreen(fData, kwArgs):
    """ This method will override the videos aspect ratio forcing users preference.
    May be undesirable to stretch video. """
    if 'widescreen' in kwArgs.keys():
        fData['widescreen'] = kwArgs['widescreen']

        if fData['widescreen'] not in ('True', 'true', True):
            fData['widescreen'] = False
        else:
            fData['widescreen'] = True

        yesStrs = ('Yes', 'Y', 'yes', 'y')
        noStrs = ('No', 'N', 'no', 'n')

        for fName in fData['vDatas']:
            if fData['widescreen'] is True:
                if fData['vDatas'][fName]['ratio'] <= 1.3333333 and fData['ask_confirmation'] is True:
                    question = '''\nWe have detected that your video is not wide screen format.
Continuing may distort the final video.
Would you like to continue encoding your video as wide screen?'''


                    while True:
                        print(question)
                        try:
                            answer = input('> Yes or No:\n')
                        except KeyboardInterrupt:
                            sExit()
                        else:
                            if answer in yesStrs:
                                fData['widescreen'] = True
                                break
                            elif answer in noStrs:
                                fData['widescreen'] = False
                                break

            else:
                if not fData['vDatas'][fName]['ratio'] <= 1.3333333 and fData['ask_confirmation'] is True:
                    question = '''\nWe have detected that your video is wide screen format.
Continuing may distort the final video.
Would you like to continue encoding your video as not wide screen?'''


                    while True:
                        print(question)
                        try:
                            answer = input('> Yes or No:\n')
                        except KeyboardInterrupt:
                            sExit()
                        else:
                            if answer in yesStrs:
                                fData['widescreen'] = False
                                break
                            elif answer in noStrs:
                                fData['widescreen'] = True
                                break

    else:
        if fData['make_videos']:
            fData['widescreen'] = None
            for fName in fData['vDatas']:
                # print(fData['vDatas'][fName]['ratio'])
                if fData['vDatas'][fName]['ratio'] <= 1.3333333:
                    # print('4:3')
                    fData['widescreen'] = False
                else:
                    # print('16:9')
                    fData['widescreen'] = True
        else:
            fData['widescreen'] = True
    return fData


## -----------------** setsize kwarg **-----------------##
def set_size(fData, kwargs):
    """  Function will look for setsize in kwargs and parse setsize
    info fData to be returned.  """
    if 'setsize' in kwargs.keys():
        fData['setsize'] = kwargs['setsize']
        if not 'bitrate' in kwargs.keys():
            print('If you set the size of your video you must specify your desired bitrate.')
            sExit()
        elif not 'maxbit' in kwargs.keys():
            print('If you set the size of your video you must specify your desired maximum bitrate.')
            sExit()
        elif not 'buffersize' in kwargs.keys():
            print('If you set the size of your video you must specify your desired buffer size.')
            sExit()
        else:
            width, height = kwargs['setsize'].split('x')

            fData['sizs'] = [{'width': width,
                              'height': height,
                              'bit_rate': int(kwargs['bitrate']),
                              'max_bit': int(kwargs['maxbit']),
                              'buffer_size': int(kwargs['buffersize'])}]
    else:
        if fData['make_videos']:
            fData['setsize'] = None
            fData['sizs'] = []
            fData['sizs'] = rez_limit(sizes(fData['setsize'], fData['widescreen']), kwargs)
    return fData


## --------- ** resolution limiter kwarg  **---------- ##
def rez_limit(sizeObj, kwargs):
    """ This function will try to limit the size of the resolutions to be
    encoded by a filter given by the user in the form of an array.

      Example:
          python3.7 mkH5videos.py rez_limit=['hd', 'hq', 'sd']"""
    if 'rez_limit' in kwargs.keys():
        print(kwargs['rez_limit'])
        limiter = json.loads(json.dumps(kwargs['rez_limit']))
        print(f'\n\n{limiter}\n\n')
        return [y for x, y in sizeObj.items() if x in limiter]
    else:
        return [y for y in sizeObj.values()]


## ------------------** cores kwarg **------------------##
def set_cores(fData, kwargs):
    """  Function will look for set_cores in kwargs and parse set_cores
    info fData to be returned.  """
    if 'threds' in kwargs.keys():
        fData['cores'] = kwargs['threds']
    else:
        fData['cores'] = cpu_count() - 1
    return fData


## ------------------** playerSize kwarg **------------------##
def set_player_size(fData, kwargs):
    """  Function will look for set_player_size in kwargs and parse
    set_player_size info fData to be returned.  """
    if 'playersize' in kwargs.keys():
        fData['playerSize'] = kwargs['playersize'].split('x')
    else:
        if fData['widescreen'] is False:
            fData['playerSize'] = [400, 300]
        else:
            fData['playerSize'] = [400, 240]
    return fData


## ------------------** thumbSize kwarg **------------------##
def set_thumb_size(fData, kwargs):
    """  Function will look for set_thumb_size in kwargs and parse
    set_thumb_size info fData to be returned.  """
    if 'thumbsize' in kwargs.keys():
        fData['thumbSize'] = kwargs['thumbsize'].split('x')
    else:
        if fData['widescreen'] is False:
            fData['thumbSize'] = [120, 90]
        else:
            fData['thumbSize'] = [120, 72]
    return fData


## ------------------** json kwarg **-------------------##
def set_json(fData, arg, kwargs):
    """  Function will look for set_json in kwargs and parse set_json
    info fData to be returned.  """
    if 'json' in kwargs.keys():
        fData['json'] = kwargs['json']
        if fData['json'] in (False, 'False', 'false'):
            fData['json'] = False

        if fData['json'] in (True, 'True', 'true'):
            fData['json'] = True
    else:
        if '-json' in arg:
            fData['json'] = True
        else:
            fData['json'] = False
    return fData


## ------------------** html kwarg **-------------------##
def set_html(fData, arg, kwargs):
    """  Function will look for set_htm in kwargs and parse set_htm
    info fData to be returned.  """
    if 'html' in kwargs.keys():
        fData['html'] = kwargs['html']
        if fData['html'] in (False, 'False', 'false'):
            fData['html'] = False

        if fData['html'] in (True, 'True', 'true'):
            fData['html'] = True
    else:
        if '-html' in arg:
            fData['html'] = True
        else:
            fData['html'] = False
    return fData


## ------------------** jScript kwarg **-------------------##
def set_javascript(fData, arg, kwargs):
    """  Function will look for set_javascript in kwargs and parse
    set_javascript info fData to be returned.  """
    if 'jScript' in kwargs.keys():
        fData['jScript'] = kwargs['jScript']
        if fData['jScript'] in (False, 'False', 'false'):
            fData['jScript'] = False

        if fData['jScript'] in (True, 'True', 'true'):
            fData['.jScript'] = True
    else:
        if '-javascript' in arg:
            fData['jScript'] = True
        else:
            fData['jScript'] = False
    return fData


## ------------------** getJS kwarg **-------------------##
def get_javascript(fData, kwargs):
    """  Function will look for get_javascript in kwargs and parse
    get_javascript info fData to be returned.  """
    if 'getJS' in kwargs.keys():
        fData['getJS'] = kwargs['getJS']
        if fData['getJS'] in (False, 'False', 'false'):
            fData['getJS'] = False

        if fData['getJS'] in (True, 'True', 'true'):
            fData['getJS'] = True
            print('here')
            return mkJS('', True)
    else:
        fData['getJS'] = False
    return fData


## ------------------** css kwarg **-------------------##
def set_css(fData, arg, kwargs):
    """  Function will look for set_css in kwargs and parse set_css
    info fData to be returned.  """
    if 'css' in kwargs.keys():
        fData['css'] = kwargs['css']
        if fData['css'] in (False, 'False', 'false'):
            fData['css'] = False

        if fData['css'] in (True, 'True', 'true'):
            fData['css'] = True
    else:
        if '-css' in arg:
            fData['css'] = True
        else:
            fData['css'] = False
    return fData


## ------------------** php kwarg **-------------------##
def set_php(fData, arg, kwargs):
    """  Function will look for set_php in kwargs and parse set_php
    info fData to be returned.  """
    if 'php' in kwargs.keys():
        fData['php'] = kwargs['php']
        if fData['php'] in (False, 'False', 'false'):
            fData['php'] = False

        if fData['php'] in (True, 'True', 'true'):
            fData['php'] = True
    else:
        if '-php' in arg:
            fData['php'] = True
        else:
            fData['php'] = False
    return fData


## ------------------** allWeb kwarg **-------------------##
def set_all_web(fData, arg, kwargs):
    """  Function will look for set_all_web in kwargs and parse
    set_all_web info fData to be returned.  """
    if 'allWeb' in kwargs.keys():
        fData['allWeb'] = kwargs['allWeb']
        if fData['allWeb'] in (False, 'False', 'false'):
            fData['allWeb'] = False

        if fData['allWeb'] in (True, 'True', 'true'):
            fData['allWeb'] = True
    else:
        if '-all-web' in arg:
            fData['allWeb'] = True
        else:
            fData['allWeb'] = False
    return fData


## ------------------** /dev/null kwarg **-------------------##
def set_dev_null():
    """  Function will look for set_dev_null in kwargs and parse
    set_dev_null info fData to be returned.  """
    # give location for first pass encoding based on OS
    if oName == 'nt':
        return 'Null'
    else:
        return '/dev/null'


def parse_video_info(vFiles):
    """ This function will read a video file and parse it's metadata
    for it's video information to be used when creating our new videos."""
    retDict = {}
    rmThese = ['-html', '-javascript', '-css', '-php', '-json', '-all-web']
    vFiles = [x for x in vFiles if x not in rmThese]
    print(vFiles)
    for vidFile in vFiles:                    
        if not oExists(vidFile):
            print(f'The file {vidFile} does not exist. Skipping...\n')
            # sExit()
        else:
            if not oIsabs(vidFile):
                vidFile = oJoin(oGetcwd(), vidFile)

            _bName = oSplitext(oBasename(vidFile))
            _oData = getVidInfo(vidFile)

            try:
                _r = round(float(_oData['streams'][0]['width']) / float(_oData['streams'][0]['height']), 3)
            except Exception as e:
                print(e)
                if _oData['streams'][1]['coded_width'] > 0 and _oData['streams'][1]['coded_height'] > 0:
                    _r = round(float(_oData['streams'][1]['coded_width']) / float(_oData['streams'][1]['coded_height']), 3)
                else:
                    _aR = _oData['streams'][1]['display_aspect_ratio'].split(':')
                    _r = round(float(_aR[0]) / float(_aR[1]), 3)

            _isMov = bool(_bName[1] == 'mov')
    
            retDict[_bName[0]] = {'name': _bName[0],
                                  'loc': vidFile,
                                  'size': oGetsize(vidFile),
                                  'bitRate': _oData['format']['bit_rate'],
                                  'duration': _oData['format']['duration'],
                                  'isMov': _isMov,
                                  'ratio': _r}
    return retDict


def sizes(setsize=None, widescreen=True):
    '''This function sets the default sizes bit rates and buffer sizes to be used.'''

    if widescreen:
        if setsize is None:
            # 16:9
            setsize = {'xd': {'width': 2560, 'height': 1440, 'bit_rate': 7200000, 'max_bit': 8400000, 'buffer_size': 15000000},
                       'hd': {'width': 1920, 'height': 1080, 'bit_rate': 3600000, 'max_bit': 4200000, 'buffer_size': 8400000},
                       'hq': {'width': 1280, 'height': 720, 'bit_rate': 1800000, 'max_bit': 2000000, 'buffer_size': 4200000},
                       'sd': {'width': 720, 'height': 406, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
                       'lq': {'width': 480, 'height': 270, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
                       'm': {'width': 320, 'height': 180, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}}
        return setsize
    else:
        if setsize is None:
            # 4:3
            setsize = {'xd': {'width': 1920, 'height': 1440, 'bit_rate': 7200000, 'max_bit': 8400000, 'buffer_size': 15000000},
                       'hd': {'width': 1440, 'height': 1080, 'bit_rate': 3600000, 'max_bit': 4200000, 'buffer_size': 8400000},
                       'hq': {'width': 1024, 'height': 768, 'bit_rate':1800000, 'max_bit': 2000000, 'buffer_size': 4200000},
                       'sd': {'width': 640, 'height': 480, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
                       'lq': {'width': 400, 'height': 300, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
                       'm': {'width': 272, 'height': 204, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}}
        return setsize


def imgResize(wantW, wantH, origW, origH):
    """ Function returns the best size for either the wanted height
    or wanted width if an image while maintaining the aspect ratio 
    of the image."""
    r = wantH / wantW
    if origW * r > origH:
        r = wantW / origW
    else:
        r = wantH / origH

    origW *= r
    origH *= r
    return origW, origH


def getVidInfo(vPath):
    '''This function gets json data from ffprobe'''
    # print(vPath)
    if oExists(vPath):
        command = ['ffprobe', '-loglevel', 'quiet', '-print_format', 
                   'json', '-show_format', '-show_streams', vPath]
        pipe = subProc.Popen(command, stdout=subProc.PIPE,
                             stderr=subProc.STDOUT)
        out, err = pipe.communicate()
        if not err is None:
            print(f'Error: {str(err)}')
        return json.loads(out)
    return None


def mkJson(baseDir):
    """ This function will build the JSON file based on the continence of if the 
    webVideos directory. It will search the WebVideos directory recursively
    in it's attempt to build or rebuild the JSON file. This function attempts to
    be a non destructive to previous data as possible. Particularly the videos 
    about string variable."""

    jsonFile = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
    # print(jsonFile)
    jData = {}
    if oExists(jsonFile):
        print('jsonExixts')
        try:
            jsData = open(jsonFile)
            jData = json.load(jsData)
            jsData.close()
        except ValueError:
            jData = json.loads(jsonFile)
        else:
            jFexists = True
    else:
        jFexists = False

    # print(jData['settings'])

    oData = MutableDict()
    # fnd = rCompile(r'(.*-([0-9]{3,4})x[0-9]{3,4}.*)')
    fnd = rCompile(r"(.*-[0-9]{3,4}x([0-9]{3,4}).*)")
    topDir = oJoin(baseDir, 'mkH5videos', 'webVideos')
    if not jFexists:
        oData['settings'] = {}
        oData['settings']['thumbColor'] = '#33daff'
        oData['settings']['trackColor'] = None
        oData['settings']['trackColorDone'] = '#184A50'

        oData['settings']['videoOrder'] = 'false'
        oData['settings']['playlist'] = 'false'
        oData['settings']['uSetRez'] = 'SD'
        oData['settings']['rezColorUp'] = '#FD5600'
        oData['settings']['rezColorSelect'] = '#458CCB'
        oData['settings']['rezColorOver'] = '#ffffff'
        oData['settings']['shoCtrlOnStart'] = 'visible'
        # oData['settings']['buttonW'] = noPxNum(player.style.height) * 0.10 + 10 + 'px'
        # oData['settings']['buttonH'] = noPxNum(player.style.height) * 0.10 + 'px'
        oData['settings']['bufferColor'] = '#ffffff'
        oData['settings']['btnColOver'] = '#72ABDD'
        oData['settings']['btnBgTop'] = '#040202'
        oData['settings']['btnBgTopAlpha'] = 1
        oData['settings']['btnBgBottom'] = '#10100A'
        oData['settings']['btnBgBottomAlpha'] = 1
        oData['settings']['nowPlayColor'] = '#91C7F6'
        oData['settings']['volOffset'] = 10
        oData['settings']['hideCtrlsTime'] = 3

    else:
        # oData['settings'] = {}
        oData['settings'] = jData['settings']

    for movieName in sorted(oListdir(topDir)):
        if oIsdir(oJoin(topDir, movieName)):
            oData[movieName] = {}
            oData[movieName]['name'] = fix_filename(movieName, True)
            if jData != {} and movieName in jData.keys() and not jData[movieName]['about'] == 'About your video?':
                oData[movieName]['about'] = jData[movieName]['about']
            else:
                oData[movieName]['about'] = 'About your video?'
            oData[movieName]['loc'] = oJoin('webVideos', movieName)

        for dataDir in sorted(oListdir(oJoin(topDir, movieName))):
            if oIsdir(oJoin(topDir, movieName, dataDir)):
                oData[movieName][dataDir] = {}
                p = 0
                for data in sorted(oListdir(oJoin(topDir, movieName, dataDir))):
                    if iSfile(oJoin(topDir, movieName, dataDir, data)):
                        if dataDir in ('mp4', 'webm'):

                            match = findall(fnd, data)

                            if str(match[0][1]) == '1440':
                                oData[movieName][dataDir]['XD'] = oJoin(dataDir, match[0][0])

                            elif str(match[0][1]) == '1080':
                                oData[movieName][dataDir]['HD'] = oJoin(dataDir, match[0][0])

                            elif str(match[0][1]) in ('720', '768'):
                                oData[movieName][dataDir]['HQ'] = oJoin(dataDir, match[0][0])

                            elif str(match[0][1]) in ('406', '480'):
                                oData[movieName][dataDir]['SD'] = oJoin(dataDir, match[0][0])

                            elif str(match[0][1]) in ('270', '300'):
                                oData[movieName][dataDir]['LQ'] = oJoin(dataDir, match[0][0])

                            elif str(match[0][1]) in ('180', '204'):
                                oData[movieName][dataDir]['M'] = oJoin(dataDir, match[0][0])

                        if dataDir == 'images':
                            oData[movieName][dataDir]['p_%s'%p] = {}
                            loc = oJoin('webVideos', movieName, dataDir, data)
                            try:
                                im = Image.open(oJoin(topDir, movieName, dataDir, data))
                                width, height = im.size
                                oData[movieName][dataDir][f'p_{p}']['loc'] = loc
                                oData[movieName][dataDir][f'p_{p}']['width'] = width
                                oData[movieName][dataDir][f'p_{p}']['height'] = height
                            except UnidentifiedImageError:
                                oRemove(oJoin(topDir, movieName, dataDir, data))
                            p += 1

                    else:
                        if oIsdir(oJoin(topDir, movieName, dataDir, data)):
                            oData[movieName][data] = {}
                            for x, thumb in enumerate(sorted(oListdir(oJoin(topDir, movieName, dataDir, data)))):
                                if iSfile(oJoin(topDir, movieName, dataDir, data, thumb)):
                                    oData[movieName][data]['t_%s'%x] = {}
                                    loc = oJoin('webVideos', movieName, dataDir, data, thumb)
                                    try:
                                        im = Image.open(oJoin(topDir, movieName, dataDir, data, thumb))
                                        width, height = im.size
                                        oData[movieName][data][f't_{x}']['loc'] = loc
                                        oData[movieName][data][f't_{x}']['width'] = width
                                        oData[movieName][data][f't_{x}']['height'] = height
                                    except UnidentifiedImageError:
                                        oRemove(oJoin(topDir, movieName, dataDir, data, thumb))

    jsonPath = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
    jFile = open(jsonPath, 'w')
    jFile.write(json.dumps(oData, indent=4))
    jFile.close()


def cmd_read_out(cmd, print_=True, inline=True, message=''):
    """ Read progress in real time to stdout and log output for return. """
    process = subProc.Popen(cmd, bufsize=1, universal_newlines=True, stdout=subProc.PIPE, stderr=subProc.STDOUT)
    outPut = ''
    for line in iter(process.stdout.readline, ''):
        if line != '':
            outPut = f'{outPut}\n{line.strip()}'
            if print_:
                _line = f'{message}{line}'
                if inline:
                    syStdout.flush()
                    syStdout.write(_line.replace('\n', '\r').encode())
                else:
                    syStdout.buffer.write(_line.encode())
        syStdout.flush() # please see comments regarding the necessity of this line 
    process.wait()
    errcode = process.returncode
    return (outPut, True) if errcode == 0 else (outPut, errcode)



_helpNotes = R"""
-------------------------------------------------------------------------

Notes:

  ** These are likely to change quite a bit as there is much testing to be done. **
  Default 16:9 Screen - size, bitrate, maxbit, buffersize:
    {'xd': {'width': 2560, 'height': 1440, 'bit_rate': 7200000, 'max_bit': 8400000, 'buffer_size': 15000000},
     'hd': {'width': 1920, 'height': 1080, 'bit_rate': 3600000, 'max_bit': 4200000, 'buffer_size': 8400000},
     'hq': {'width': 1280, 'height': 720, 'bit_rate': 1800000, 'max_bit': 2000000, 'buffer_size': 4200000},
     'sd': {'width': 720, 'height': 406, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
     'lq': {'width': 480, 'height': 270, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
     'm':  {'width': 320, 'height': 180, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}}

  Default 4:3 Screen - size, bitrate, maxbit, buffersize:
    {'xd': {'width': 1920, 'height': 1440, 'bit_rate': 7200000, 'max_bit': 8400000, 'buffer_size': 15000000},
     'hd': {'width': 1440, 'height': 1080, 'bit_rate': 3600000, 'max_bit': 4200000, 'buffer_size': 8400000},
     'hq': {'width': 1024, 'height': 768, 'bit_rate':1800000, 'max_bit': 2000000, 'buffer_size': 4200000},
     'sd': {'width': 640, 'height': 480, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
     'lq': {'width': 400, 'height': 300, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
     'm':  {'width': 272, 'height': 204, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}}
  
  When setting your own sizes the width and height must be divisible by 2. The 
  bitrate has to be >= 30000. The maxbit must be > bitrate and the buffersize 
  should be > maxbit.

  This program does a two pass encoding for both formats. On mp4 it uses 
  qt-faststart to move all the meta data to the beginning for streaming.

  It also creates 5 video plaice holder images at different times so you
  have more choices of place holders. It does the same for thumbnails.

  Reading:
    DuckDuckGo FFmpeg and read everything you can.

    The commands used to write the videos were taken and adapted from:
    https://github.com/adexin-team/refinerycms-videojs/wiki/Encoding-files-to-.webm-(VP8)-and-.mp4-(h.264)-using-ffmpeg

  Tips:
    Make this script run from anywhere in the terminal:Bash, so you
      don't have to type /home/$USER/my/path/to/mkH5videos.py
      https://ubuntuforums.org/showthread.php?t=1074736

    Use the \\ to break up the arguments for better readability.
      $ mkH5videos.py ~/Desktop/myVid1.mp4 \\
      > ~/Videos/AwesomeVideos/myVid2.webm \\
      > widescreen=true setsize=1920x1080 \\
      > json=true html=true

    Make the json file and edit the settings before making the html and css
    as this program will read the json settings and set colors accordingly.


    When using the rez_limiter to limit the encoding resolutions remember that
    the terminal will break the resolution list unless there are no spaces 
    between comas.

"""    

def mkHTML(baseDir, playerSize):
    """ This function will make the HTML5 file. """
    jsonFile = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
    print(jsonFile)
    if oExists(jsonFile):
        try:
            jsData = open(jsonFile)
            jData = json.load(jsData)
            jsData.close()
        except ValueError:
            jData = json.loads(jsonFile)

        for key in sorted(jData):
            if not key == 'settings':
                print(key)
                theKey = key
                break


        htmlPath = oJoin(baseDir, 'mkH5videos', 'movie.html')
        jsonPath = oJoin('scripts', 'mkH5videos.json')
        mp4Def = oJoin(f"{jData[theKey]['loc']}", f"{jData[theKey]['mp4']['SD']}")
        webmDef = oJoin(f"{jData[theKey]['loc']}", f"{jData[theKey]['webm']['SD']}")
        vidThumb = oJoin(f"{jData[theKey]['images']['p_0']['loc']}")
        buffColor = jData['settings']['bufferColor']

        buffColor = buffColor if 1 in [1 for x in ['"', "'"] if x in buffColor] else f"'{buffColor}'"

        defSet = '\n\t\t\t\t\t      {\n'
        defSet += f"\t\t\t\t\t       'videoOrder': {jData['settings']['videoOrder']},\n"
        defSet += f"\t\t\t\t\t       'uSetRez': '{jData['settings']['uSetRez']}',\n"
        defSet += f"\t\t\t\t\t       'rezColorUp': '{jData['settings']['rezColorUp']}',\n"
        defSet += f"\t\t\t\t\t       'rezColorSelect': '{jData['settings']['rezColorSelect']}',\n"
        defSet += f"\t\t\t\t\t       'rezColorOver': '{jData['settings']['rezColorOver']}',\n"
        defSet += f"\t\t\t\t\t       'shoCtrlOnStart': '{jData['settings']['shoCtrlOnStart']}',\n"
        defSet += f"\t\t\t\t\t       'btnColUp': '{jData['settings']['btnColUp']}',\n"
        defSet += f"\t\t\t\t\t       'btnColOver': '{jData['settings']['btnColOver']}',\n"
        defSet += f"\t\t\t\t\t       'btnBgTop': '{jData['settings']['btnBgTop']}',\n"
        defSet += f"\t\t\t\t\t       'btnBgTopAlpha': {jData['settings']['btnBgTopAlpha']},\n"
        defSet += f"\t\t\t\t\t       'btnBgBottom': '{jData['settings']['btnBgBottom']}',\n"
        defSet += f"\t\t\t\t\t       'btnBgBottomAlpha': {jData['settings']['btnBgBottomAlpha']},\n"
        defSet += f"\t\t\t\t\t       'nowPlayColor': '{jData['settings']['nowPlayColor']}',\n"
        defSet += f"\t\t\t\t\t       'volOffset': {jData['settings']['volOffset']},\n"
        defSet += f"\t\t\t\t\t       'hideCtrlsTime': {jData['settings']['hideCtrlsTime']},\n"
        defSet += f"\t\t\t\t\t       'videoOrder': {jData['settings']['videoOrder']},\n"
        defSet += f"\t\t\t\t\t       'bufferColor': {buffColor},\n"
        defSet += f"\t\t\t\t\t       'playlist': {jData['settings']['playlist']}\n"
        defSet += '\t\t\t\t\t      }'

        htmlFile = f"""<!DOCTYPE html>
<html>
    <head>
        <title>Your HTML5 Videos</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="scripts/mkH5videos.css" ></link>
        <script src="scripts/mkH5videos.js" type="application/javascript" charset="utf-8"></script>
        <script src="scripts/TweenMax.min.js" type="text/javascript" charset="utf-8"></script>
        <script>
            window.addEventListener("load", function(){{
                var vid, player;
                // get the video vars
                vid = document.getElementById('{theKey}');
                player = document.getElementById('player');
                // mkH5video(args, object or nothing); The object is set to the json['settings']
                mkH5video(player, vid, '{jsonPath}', {defSet});
            }});
        </script>
    </head>
    <body>
        <!-- use inline style for positioning. JS is loaded before CSS and needs to do math for controls -->
        <div id="player" style="position: absolute; top: 100px; left: 100px; width:{playerSize[0]}px; height:{playerSize[1]}px;">
            <!-- set height and width for JS math -->
            <!-- video id dynamically changes when video is loaded. Default is set to first video in json file.-->
            <!-- For the resolution changer function to work the initial video id must be set to the object key of the video -->
            <video id="{theKey}" class="video-js vjs-default-skin"
              preload="metadata" width="{playerSize[0]}px" height="{playerSize[1]}px"
              poster="{vidThumb}">
              <source src="{mp4Def}" type='video/mp4'>
              <source src="{webmDef}" type='video/webm'>
            </video>
        </div>
    </body>
</html>"""

        print(htmlFile)
        htmlPath = oJoin(baseDir, 'mkH5videos', 'mkH5videos.html')
        hFile = open(htmlPath, 'w')
        hFile.write(htmlFile)
        hFile.close()
    else:
        print('cannot create HTML file. You must first make the json file.')


def mkJS(baseDir, getJS):
    """  """
    jsFile = R"""'use strict';if(!window.gsap){let e=document.createElement("script");e.type="text/javascript",e.src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js",document.getElementsByTagName("head")[0].appendChild(e)}function mkH5video(e,t,l,s,n){var d=Math.abs,o=Math.floor,r=Math.ceil,p=Math.round;function y(t){let e,l,i=t.target||t.srcElement||window.event;"unMuteTop"==i.id?(e=i.id.replace("Top","1_btn"),l=i.id.replace("Top","2_btn"),e=document.getElementById(e),l=document.getElementById(l),gsap.to(e,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0}),gsap.to(l,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0})):(e=i.id.replace("Top","_btn"),e=document.getElementById(e),gsap.to(e,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0}))}function a(t){let e,l,i=t.target||t.srcElement||window.event;"unMuteTop"==i.id?(e=i.id.replace("Top","1_btn"),l=i.id.replace("Top","2_btn"),e=document.getElementById(e),l=document.getElementById(l),gsap.to(e,{duration:.5,fill:s.btnColUp,scale:1,x:0,y:0,overwrite:!0}),gsap.to(l,{duration:.5,fill:s.btnColUp,scale:1,x:0,y:0,overwrite:!0})):(e=i.id.replace("Top","_btn"),e=document.getElementById(e),gsap.to(e,{duration:.5,fill:s.btnColUp,scale:1,x:0,y:0,overwrite:!0}))}function h(e,t){gsap.to(ze,{duration:0,fillOpacity:e,overwrite:!0}),gsap.to(ke,{duration:0,fillOpacity:t,overwrite:!0})}function u(e){32==e.keyCode&&m()}function c(e){if(39==e.keyCode){let t=10;e.ctrlKey&&(t*=2),C("forward",t)}}function b(e){if(37==e.keyCode){let t=10;e.ctrlKey&&(t*=2),C("back",t)}}function m(){G(),(document.fullscreenElement||document.mozFullScreenElement)&&t.addEventListener("ended",F,!1),t.paused?(ge=!0,t.play(),t.volume=tt.value/100,!be&&(Ee.style.zIndex="10",Ee.style.visibility="hidden",Ne.style.zIndex="12",Ne.style.visibility="visible")):(ge=!1,t.pause(),t.volume=tt.value/100,!be&&(Ee.style.zIndex="12",Ee.style.visibility="visible",Ne.style.zIndex="10",Ne.style.visibility="hidden"))}function g(){t.muted?(tt.value=100*t.volume,t.muted=!1,Ge.style.zIndex="10",Ge.style.visibility="hidden",Qe.style.zIndex="12",Qe.style.visibility="visible",gsap.to(Je,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0}),gsap.to($e,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0})):(t.muted=!0,tt.value=0,Ge.style.zIndex="12",Ge.style.visibility="visible",Qe.style.zIndex="10",Qe.style.visibility="hidden",gsap.to(Ye,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0}))}function v(){document.querySelector("meta[name=\"viewport\"]").content="width=device-width, height=device-height, initial-scale=1",ot.removeEventListener("click",v,!1),ot.addEventListener("click",x,!1),e.requestFullscreen?e.requestFullscreen():e.mozRequestFullScreen?e.mozRequestFullScreen():e.webkitRequestFullscreen?e.webkitRequestFullscreen():e.msRequestFullscreen?e.msRequestFullscreen():O("Your browser does not support full screen.");let t=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;if(t===void 0||Object.getPrototypeOf(t)!==Object.prototype){let e=document.getElementById("aboutBG"),t=document.getElementById("pListHolder");null!==t&&(t.style.visibility="hidden"),null!==e&&(e.style.visibility="hidden"),M(),f("mk")}}function x(){document.querySelector("meta[name=\"viewport\"]").content=oe,console.log(document.querySelector("meta[name=\"viewport\"]").content),ot.removeEventListener("click",x,!1),ot.addEventListener("click",v,!1),document.exitFullscreen?document.exitFullscreen():document.webkitExitFullscreen?document.webkitExitFullscreen():document.mozCancelFullScreen?document.mozCancelFullScreen():document.msExitFullscreen?document.msExitFullscreen():O("Your browser does not support full screen.");let t=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;if(t===void 0||Object.getPrototypeOf(t)===Object.prototype){let t=document.getElementById("aboutBG"),l=document.getElementById("pListHolder");t.style.visibility="visible",null!==l&&(l.style.visibility="visible"),D(),document.getElementById(e.id).scrollIntoView()}}function f(e){let t=["fullscreenchange","mozfullscreenchange","MSFullscreenChange","webkitfullscreenchange"];if("mk"==e)for(let e in t)document.addEventListener(t[e],w,!1);else for(let e in t)document.removeEventListener(t[e],w,!1)}function w(){let l=document.fullscreenElement||document.webkitFullscreenElement||document.mozFullScreenElement||document.msFullscreenElement;(null===l||l===void 0||Object.getPrototypeOf(l)===Object.prototype)&&(e.style.height==fe+"px"||t.style.width==xe+"px")&&(x(),f("rm"),document.getElementById(e.id).scrollIntoView())}function C(e,l){let i=t.duration*(Ue.value/100);e!=null&&-1!=["back","forward"].indexOf(e)&&l!=null&&0==l%1&&("forward"==e?i=t.currentTime+l:i=t.currentTime-l),t.currentTime=i}function E(){let e,l=0,i=t.duration;for(0<=t.buffered.length-1&&(e=t.buffered.end(t.buffered.length-1),B()),te=0;te<=t.buffered.length-1;te++){let e=p(100*(t.buffered.start(te)/i)),n=t.buffered.end(te)-t.buffered.start(te),d=p(100*(n/i));if(l+=n,null===document.getElementById("bufProg_"+te)){let t=document.createElement("div");t.id="bufProg_"+te,t.className="buffProg",xt.appendChild(t),t.style.position="absolute","[object Array]"===Object.prototype.toString.call(s.bufferColor)?t.style.backgroundColor=S(s.bufferColor):"rainbow"==s.bufferColor?(console.log(s.bufferColor),t.style.backgroundColor=S()):(console.log("fuck"),t.style.backgroundColor=s.bufferColor),O("Progress Buffer backgroundColor = "+t.style.backgroundColor),t.style.height=xt.style.height,t.style.alpha=.75,t.style.bottom="0px",t.style.display="inline-block",t.style.left=e+"%",t.style.width=d+"%"}else{let t=document.getElementById("bufProg_"+te);t.style.left=e+"%",t.style.width=d+"%"}if("firefox"==we&&r(l)>=.95*o(i)){let e=document.getElementById("bufProg_"+te);e.style.width="100%"}}}function S(e){if(e===void 0||"[object Array]"!==Object.prototype.toString.call(e)){let e=["A","B","C","D","E","F"],t="";for(te=6;1<=te;te--)t+=0==(o(498*Math.random())+1)%2?e[o(6*Math.random())]:o(10*Math.random());return"#"+t}else{let t=[];for(let l in e)e[l].match(/^#?[A-Fa-f0-9]{6}[\s]{0,1}$/g)&&("#"===e[l].substring(0,1)?t.push(e[l].toUpperCase()):t.push("#"+e[l].toUpperCase()));return t[z(0,t.length-1)]}}function z(e,t){return o(Math.random()*(1+t-e))+e}function A(){t.volume=tt.value/100}function B(){t.ended&&(t.currentTime=0,ge=!1);let e=t.currentTime*(100/t.duration),l=o(t.currentTime/60),i=o(t.currentTime-60*l),s=o(l/60),n=o(t.duration/60),d=p(t.duration-60*n),r=o(n/60);isNaN(d)?Ue.value=1:(Ue.value=e,10>i&&(i="0"+i),10>d&&(d="0"+d),10>l&&(l="0"+l),10>n&&(n="0"+n),1<=r?(60<=l&&(l-=60*r,10>l&&(l="0"+l)),n-=60*r,Ve.innerHTML=s+":"+l+":"+i+" | ",je.innerHTML=r+":"+n+":"+d):(Ve.innerHTML=l+":"+i+" | ",je.innerHTML=n+":"+d))}function I(){be=!1,t.style.cursor="default",ye.style.visibility="visible",ft.play(),t.style.cursor="default"}function T(){be=!0,ft.reverse(),t.style.cursor="none"}function L(){St.play()}function k(){St.reverse()}function H(e){return e=e.replace("px","").replace("%",""),+e}function O(e){n&&console.log(e)}function _(e){(e===void 0||null===e||!0!==e)&&(e=!1);let t,l={chrome:/Chrome/.test(navigator.userAgent),safari:/Safari/.test(navigator.userAgent),iPad:/iPad/.test(navigator.userAgent),iPhone:/iPhone/.test(navigator.userAgent),droid:/Android/.test(navigator.userAgent),firefox:/Firefox/.test(navigator.userAgent)},i="";for(let s in l)if(e)i=""===i?s:i+", "+s;else{if(l[s])return s;t=!0}return e?i:t?"The User-Agent your looking for is not in the list of User-Agents to search for. You can add it manually in the getUserAgent.userArr Object.":void 0}function N(e,t){if(e<t){return-(t-e+.25*e)+"px"}return"3px"}function D(){re=!1,G(),document.getElementById("showSplash_btn")&&(document.getElementById("showSplash_btn").removeEventListener("click",j,!1),bt.removeChild(document.getElementById("splashBG"))),t.removeEventListener("ended",F),t.style.height=he,t.style.width=ae,e.style.height=he,e.style.width=ae,e.style.top=ue,e.style.left=ce,ye.style.width=H(ae)+"px",ye.style.height=H(ne)+10+"px",ye.style.position="absolute",ye.style.bottom="0px",ye.style.left="0px",ct.style.width=ae,ct.style.height=ye.style.height,vt.style.width=ye.style.width,vt.style.height=ye.style.height,bt.style.bottom="0px",bt.style.right="0px",bt.style.height=ne,mt.style.bottom="0px",gt.style.left=H(Ge.style.width)+5+"px",gt.style.bottom=mt.style.bottom,Ee.style.top="0px",Ee.style.left="0px",Ne.style.top="0px",Ne.style.left="0px",ot.style.left=H(gt.style.left)+H(gt.style.width)+"px",ot.style.bottom="0px",Ge.style.bottom="0px",Qe.style.bottom="0px",tt.style.bottom=H(Ge.style.height)+H(mt.style.height)-H(ne)+s.volOffset+"px",tt.style.left=H(Ge.style.width)/50+"px",Ue.style.left=H(Ee.style.left)+H(Ee.style.width)+10+"px",Ue.style.top=.5*H(Ee.style.height)+H(Ue.style.height)+"px",Ue.style.width=H(ye.style.width)-H(bt.style.width)-H(Ee.style.width)-20+"px",xt.style.position=Ue.style.position,xt.style.top=H(Ue.style.top)+1+"px",xt.style.left=H(Ue.style.left)+2+"px",xt.style.zIndex=Ue.style.zIndex-1,xt.style.height="10px",xt.style.width=Ue.style.width,xt.style.margin="5px 0px 5px 0px","chrome"===we||"safari"===we||"opera"===we?(Ue.style.top="18px",xt.style.top=H(Ue.style.top)-4+"px"):Ue.style.top=.5*H(Ee.style.height)+H(Ue.style.height)+3+"px",Pe.style.top="-2px",Pe.style.right=H(bt.style.right)+H(bt.style.width)+1.25*H(de)+"px"}function M(){setTimeout(null,300),q();e.style.top="0px",e.style.left="0px",e.style.width=screen.width+"px",e.style.height=screen.height+"px",t.style.width=screen.width+"px",t.style.height=screen.height+"px",ye.style.width=screen.width+"px",ye.style.bottom="0px",ct.style.width=screen.width+"px",vt.style.width=screen.width+"px",Ue.style.left=H(Ue.style.left)+8+"px",Ue.style.width=xe-(H(bt.style.width)+H(Ue.style.left))-16+"px",Pe.style.top="-2px",xt.style.left=H(xt.style.left)+8+"px",xt.style.width=Ue.style.width,t.addEventListener("ended",F,!1)}function F(){function l(l){let e=l.target||l.srcElement||window.event,i=Et[K(e.id).replace("_id","")];(null!==ie||ie!==void 0)&&(document.getElementById(K(ie)).style.color="#fff",document.getElementById(K(ie.replace("_title","_vTitle"))).style.color="#fff"),ie=i.name+"_title",document.getElementById(K(ie)).style.color=s.nowPlayColor,t.id=K(i.name),document.getElementById(K(i.name+"_vTitle")).style.color=s.nowPlayColor,X(Q(),i),document.getElementById("fsTitle").innerHTML=i.name,document.getElementById("aboutVidName").innerHTML=i.name,V(i.about,document.getElementById("aboutVidTxt_FS")),V(i.about,document.getElementById("aboutVidTxt")),R(document.getElementById("slideShow"),i.images,i.name)}function i(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("_id","_vPic"),i=document.getElementById(K(e.id.replace("_id","_vTitle"))),s=H(i.style.width),n=H(document.getElementById(e.id.replace("_id","_titleHold")).style.width);W(i,s,n);let d=document.getElementById(l);gsap.to(d,{duration:.1,scaleX:1.15,scaleY:1.15,overwrite:!0})}function n(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("_id","_vPic"),i=document.getElementById(l);gsap.to(i,{duration:.1,scaleX:1,scaleY:1,overwrite:!0})}G(),re=!0,gsap.to(ut,{duration:.1,scaleX:1,scaleY:1,x:0,y:0,overwrite:!0}),I(),clearTimeout(le);let r=document.createElement("div");r.id="fsTitle",r.className="aboutBG",r.style.textShadow="0px 0px, 40px, #000000",r.style.position="absolute",r.style.overflow="visible",r.style.color=s.nowPlayColor,r.style.textShadow=J(s.nowPlayColor,111)?"0px 0px 20px #ffffff":"0px 0px 20px #000000";let p=document.createElement("div");p.id="aboutVidTxt_FS",p.className="aboutVidTxt_FS",p.style.position="absolute",p.style.display="block";let y=document.createElement("div");y.style.position="absolute",y.style.overflow="auto";let a=8,h=0,u=0,c=0,b=[],m=[],g="";for(let e in Et)"settings"!=e&&(h+=Object.values(Et[e].images)[0].width,b.push(Et[e]),Object.values(Et[e].images)[0].width>c&&(c=Object.values(Et[e].images)[0].width,u=Object.values(Et[e].images)[0].height),Et[e].loc.includes(t.id)&&(r.innerHTML=Et[e].name,g=Et[e].about,m=Et[e].images));h=0==h%Object.keys(m).length?c:c;let v=document.createElement("div");v.style.position="absolute",v.style.zIndex=H(e.style.zIndex)+1,v.style.height=.9*fe+"px",v.style.width=.95*xe+"px",v.style.top=.5*(fe-H(v.style.height))+"px",v.style.left=.5*(xe-H(v.style.width))+"px",v.id="splash";let x=document.createElement("div");x.style.position="absolute",x.style.zIndex=v.style.zIndex,x.style.top="0px",x.style.left="0px",x.style.width=v.style.width,x.style.height=v.style.height,x.style.background="#000000",x.style.opacity=.3333333333,x.style.borderRadius="15px 15px 15px 15px",v.appendChild(x),e.appendChild(v),r.style.zIndex=H(v.style.zIndex)+1,v.appendChild(r),r.style.fontSize=.15*fe+"%",r.style.top=H(v.style.top)+32+"px",r.style.left=H(v.style.left)+.5*(.45*H(v.style.width)-Y(r.innerHTML,r.style.fontSize+Ce))+"px",V(g,p),y.style.height=.8*H(v.style.height)+"px",y.style.width=.46*H(v.style.width)+"px",y.style.top=.18*H(v.style.height)+"px",y.style.left=H(v.style.left)+.45*H(v.style.width)-H(y.style.width)+"px",p.style.width=.98*H(y.style.width)+"px",p.style.top="0px",p.style.left="0px",p.style.color="#ffffff",p.style.fontSize=.1*fe+"%",y.appendChild(p),v.appendChild(y);let f=H(v.style.left)+o(.45*H(v.style.width))+o(H(v.style.width)-2*(H(v.style.left)+.45*H(v.style.width))),w=document.createElement("div");w.style.position="absolute",w.style.overflow="auto",w.style.width=.46*H(v.style.width)+"px",w.style.height=2*u+"px",w.style.top=H(v.style.top)+.93*H(v.style.height)-H(w.style.height)+"px",w.style.left=f+"px";let C=document.createElement("div");C.id="vHold-here",C.style.position="absolute",C.style.width=o(.98*H(w.style.width))+"px",C.style.top="0px",C.style.left="0px",C.style.zIndex=H(v.style.zIndex)+1,w.appendChild(C),v.appendChild(w);let E=0,S=-d(u);for(let e in b){0!==e&&0==e%o(H(C.style.width)/h)&&(2>H(C.style.width)/h?(w.style.height=1.5*u+"px",w.style.top=H(v.style.top)+.93*H(v.style.height)-H(w.style.height)+"px",S=S+u+a,E=.5*(H(C.style.width)-h)+a):(S=S+u+a,E=a));let t=document.createElement("div");t.style.position="absolute",t.style.top=S+"px",t.style.left=E+"px",t.style.overflow="hidden",t.style.width=Object.values(b[e].images)[0].width+"px",t.style.height=Object.values(b[e].images)[0].height+"px";let d=document.createElement("img");d.id=b[e].name+"_vPic",d.style.position="absolute",d.style.width=t.style.width,d.style.height=t.style.height,d.style.top="0px",d.style.left="0px",d.style.zIndex=H(C.style.zIndex)+1,t.appendChild(d);let r=document.createElement("p");r.id=K(b[e].name+"_vTitle"),r.className="posterTitle",r.style.position="absolute",r.style.bottom="0px",r.innerHTML=b[e].name,r.style.zIndex="15",r.style.overflow="visible",r.style.fontSize=12,r.style.color="#ffffff",null!==ie&&ie!==void 0&&K(ie)==K(b[e].name+"_title")&&(r.style.color=s.nowPlayColor),r.style.width=Y(b[e].name,r.style.fontSize+"pt "+Ce)+"px",r.zIndex=H(d.style.zIndex)+1;let p=document.createElement("div");p.id=b[e].name+"_titleHold",p.className="vidTitleHolder",p.style.position="absolute",p.style.bottom="0px",p.style.left="0px",p.style.width=H(t.style.width)+"px",p.style.height=.25*b[e].images.p_0.height+"px",p.style.zIndex="14",p.style.backgroundColor="#000000",p.style.opacity=.7,p.zIndex=H(r.style.zIndex)+1,p.appendChild(r),t.appendChild(p);let y=document.createElement("div");y.id=b[e].name+"_id",y.style.position="absolute",y.style.width=t.style.width,y.style.height=t.style.height,y.style.top="0px",y.style.left="0px",y.style.zIndex=H(r.style.zIndex)+1,t.appendChild(y),y.addEventListener("click",l,!1),y.addEventListener("mouseover",i,!1),y.addEventListener("mouseout",n,!1),E=E+h+a,C.appendChild(t),d.style.visibility="hidden";let c=Q();if(""===c)d.src=Object.values(b[e].images)[0].loc;else{d.src=c+"/"+Object.values(b[e].images)[0].loc}d.addEventListener("load",function(){d.style.visibility="visible",gsap.from(d,{opacity:0,ease:Power2.easeOut,duration:.5})})}let z=document.createElement("div");z.id="slideShow",z.style.position="absolute",z.style.overflow="hidden",z.style.width=1.25*h+"px",z.style.height=1.25*u+"px",z.style.top=H(v.style.top)+.5*(H(v.style.height)-H(w.style.height)-H(z.style.height))+"px",z.style.left=f+.5*(H(w.style.width)-H(z.style.width))+"px",v.appendChild(z),R(z,m,r.innerHTML)}function R(e,t,l){let i=H(e.style.width),n=H(e.style.height),d=[];if(null!==document.getElementById("mkSlideShow")&&void 0!==document.getElementById("mkSlideShow")){let e=document.getElementById("mkSlideShow");e.parentNode.removeChild(e)}let o=document.createElement("div");o.id="mkSlideShow",o.style.position="absolute",o.style.width=i+"px",o.style.height=n+"px",o.style.top="0px",o.style.left="0px";let r=o.cloneNode(!0);r.style.backgroundColor="#ffffff",r.style.boxShadow="0px 0px 40px 10px #ffffff",r.style.opacity=0,r.style.zIndex=1,o.appendChild(r);let p=document.createElement("div");p.style.position="absolute",p.style.width=.1*i+"px",p.style.height=.1*n+"px",p.style.top=n-H(p.style.height)+"px",p.style.left=i-H(p.style.width)+"px",p.style.zIndex="50";let h=document.createElement("div");h.id="fwdBtn",h.style.width=de,h.style.height=ne,h.style.position="absolute",h.style.visibility="visible",h.style.top="0px",h.style.left="0px";var u=se.cloneNode(!0);let c=document.createElementNS("http://www.w3.org/2000/svg","svg");c.setAttribute("xlmns","http://www.w3.org/2000/svg"),c.setAttribute("viewBox","0 0 20 15"),c.setAttribute("preserveAspectRatio","none"),c.setAttribute("height",H(ne)),c.setAttribute("width",H(de)),c.setAttribute("x","0px"),c.setAttribute("y","0px"),c.style.position="absolute";let b=document.createElementNS("http://www.w3.org/2000/svg","path");b.setAttribute("d","M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z"),b.setAttribute("fill",s.btnColUp),b.setAttribute("id","plus_btn"),b.style.zIndex=13;let m=document.createElement("div");m.id="plusTop",m.style.height=ne,m.style.width=de,m.style.position="absolute",m.style.zIndex="15",m.style.top="0px",m.style.left="0px",m.style.padding="5px",h.appendChild(m),c.appendChild(b),h.appendChild(c),h.appendChild(u),p.appendChild(h),o.appendChild(p),d.push(h);let g=document.createElement("div");g.style.position="absolute",g.style.width=.1*i+"px",g.style.height=.1*n+"px",g.style.top=n-H(g.style.height)+"px",g.style.left=H(g.style.width)-H(g.style.width)+"px",g.style.zIndex="5",g.style.transform="scale(-1,1)";let v=document.createElement("div");v.id="bckBtn",v.style.width=de,v.style.height=ne,v.style.position="absolute",v.style.visibility="visible",v.style.top="0px",v.style.left="0px";var x=se.cloneNode(!0);let f=document.createElementNS("http://www.w3.org/2000/svg","svg");f.setAttribute("xlmns","http://www.w3.org/2000/svg"),f.setAttribute("viewBox","0 0 20 15"),f.setAttribute("preserveAspectRatio","none"),f.setAttribute("height",H(ne)),f.setAttribute("width",H(de)),f.setAttribute("x","0px"),f.setAttribute("y","0px"),f.style.position="absolute";let w=document.createElementNS("http://www.w3.org/2000/svg","path");w.setAttribute("d","M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z"),w.setAttribute("fill",s.btnColUp),w.setAttribute("id","minus_btn"),w.style.zIndex=13;let C=document.createElement("div");for(let i in C.id="minusTop",C.style.height=ne,C.style.width=de,C.style.position="absolute",C.style.zIndex="15",C.style.top="0px",C.style.left="0px",C.style.padding="5px",v.appendChild(C),f.appendChild(w),v.appendChild(f),v.appendChild(x),g.appendChild(v),o.appendChild(g),e.appendChild(o),d.push(v),d){let e=d[i];e.addEventListener("mouseover",y,!1),e.addEventListener("mouseout",a,!1),e.addEventListener("click",U,!1)}let E=Object.values(t),S=document.createElement("img");S.style.visibility="hidden",S.id=l+"_"+(E.length-1);let z=Q();if(""===z)S.src=E[E.length-1].loc;else{S.src=z+"/"+E[E.length-1].loc}S.style.width=1.1*E[E.length-1].width+"px",S.style.height=1.1*E[E.length-1].height+"px",S.addEventListener("load",function(){S.style.visibility="visible",gsap.from(S,{scaleX:0,scaleY:0,opacity:0,ease:Power2.easeOut,duration:.5})});let A=document.createElement("div");A.id="imgHolder",A.style.position="absolute",A.style.width=H(S.style.width)+"px",A.style.height=H(S.style.height)+"px",A.style.top=.25*(n-H(A.style.height))+"px",A.style.left=.5*(i-H(A.style.width))+"px",A.appendChild(S),o.appendChild(A)}function U(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("Top",""),i=document.getElementById("mkSlideShow"),s=document.getElementById("imgHolder");"minus"==l?P(s,"left",i):P(s,"right",i)}function P(e,t,l){let i=H(l.style.width),s=H(l.style.height),n=e.children[0],o=H(n.id.split("_")[1]),r=n.id.split("_")[0],p=Et[K(r)].images,y=Object.values(p).length,a=Q();"left"==t&&H(e.style.left)==.5*(H(l.style.width)-H(e.style.width))&&(o--,0>o?o=y-1:o>=y&&(o=0),gsap.to(e,{left:-d(H(e.style.width)+10)+"px",duration:1,ease:Power2.easeOut,onComplete:function(){if(e.style.left=i+2+"px",""===a)n.src=Object.values(p)[o].loc;else{n.src=a+"/"+Object.values(p)[o].loc}n.addEventListener("load",function(){n.id=r+"_"+o,n.style.height=1.1*Object.values(p)[o].height+"px",n.style.width=1.1*Object.values(p)[o].width+"px",gsap.to(e,{left:.5*(i-H(n.style.width))+"px",duration:1,ease:Power2.easeIn})})}})),"right"==t&&H(e.style.left)==.5*(H(l.style.width)-H(e.style.width))&&(o++,0>o?o=y-1:o>=y&&(o=0),gsap.to(e,{left:i+10+"px",duration:1,ease:Power2.easeOut,onComplete:function(){if(e.style.left=-d(H(e.style.width)+10)+"px",""===a)n.src=Object.values(p)[o].loc;else{n.src=a+"/"+Object.values(p)[o].loc}n.addEventListener("load",function(){n.id=r+"_"+o,n.style.height=1.1*Object.values(p)[o].height+"px",n.style.width=1.1*Object.values(p)[o].width+"px",gsap.to(e,{left:.5*(i-H(n.style.width))+"px",duration:1,ease:Power2.easeIn})})}}))}function V(e,t){if(t.style.zIndex=12,e.includes("\n")){t.innerHTML="";let l=e.split("\n");for(te in l){let e=document.createElement("p");e.className="videoDesription_P",e.innerHTML=l[te],t.appendChild(e)}}else t.innerHTML=e}function j(){document.getElementById("splashBG");!0===re?G():F()}function G(){if(re=!1,gsap.to(ut,{duration:.1,scaleX:0,scaleY:0,x:.1*H(ne),y:.1*H(de),overwrite:!0}),t.removeEventListener("ended",F),document.getElementById("splash")){let e=document.getElementById("splash");e.parentNode.removeChild(e),e=null}}function q(){let e=document.getElementById("fScreenBtn"),t=document.createElement("div");t.id="splashBG",t.className="rSizeRC",t.style.position="absolute",t.style.overflow="hidden",t.style.bottom=H(e.style.bottom)+1.2*H(e.style.height)+"0px",t.style.left=e.style.left,t.style.height="0px",t.style.width=e.style.width,t.style.display="block";let l=document.createElement("div");l.id="showSplash_btn",l.style.position="absolute",l.style.bottom="0px",l.style.left="0px",l.style.height=e.style.height,l.style.width=e.style.width,l.appendChild(ht),t.appendChild(l),bt.appendChild(t),l.addEventListener("click",j,!1);let i=gsap.to(t,{duration:.1,height:H(e.style.height)+5+"px",overwrite:!0});e.addEventListener("mouseover",function(){i.play(),!1}),e.addEventListener("mouseout",function(){i.reverse(),!1}),t.addEventListener("mouseover",function(){i.play(),!1}),t.addEventListener("mouseout",function(){i.reverse(),!1})}function X(e,l){""===e?(t.poster=l.images[Object.keys(l.images)[0]].loc,t.innerHTML="<source src="+l.loc+"/"+l.mp4[s.uSetRez]+" type='video/mp4'>\n<source src="+l.loc+"/"+l.webm[s.uSetRez]+" type='video/webm'>"):(e+="/",t.poster=e+l.images[Object.keys(l.images)[0]].loc,t.innerHTML="<source src="+e+l.loc+"/"+l.mp4[s.uSetRez]+" type='video/mp4'>\n<source src="+e+l.loc+"/"+l.webm[s.uSetRez]+" type='video/webm'>"),xt.innerHTML="",t.load(),t.addEventListener("progress",E,!1),Ue.value=1}function Y(e,t){let l=Y.canvas||(Y.canvas=document.createElement("canvas")),i=l.getContext("2d");i.font=t;let s=i.measureText(e);return r(s.width)}function W(e,t,l){let i=3.5*(t/l);if(!gsap.isTweening(e)){gsap.to(e,{duration:.75*i,right:l+3+"px",delay:.2,ease:Power0.easeIn,onComplete:function(){gsap.fromTo(e,{duration:.25*i,right:-t},{right:N(l,t),ease:Sine.easeOut})}})}}function Q(){return""===s.topDir?""===Et.settings.topDir?"":Et.settings.topDir:s.topDir}function K(e){return e.replace(/ /g,"_")}function Z(e){e=e.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i,function(e,t,l,i){return t+t+l+l+i+i});var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?{r:parseInt(t[1],16),g:parseInt(t[2],16),b:parseInt(t[3],16)}:null}function J(e,t){(null==t||2>t)&&(t=111);let l,i,s,n,d;return l=Z(e.replace("#","")),i=l.r,s=l.g,n=l.b,d=(299*i+587*s+114*n)/1e3,!(d>=t)}function $(e){e.ctrlKey&&[96,107,109,61,173].includes(e.keyCode)&&(console.log(e.keyCode),window.location.reload(!1))}var ee,te;(e===void 0||null===e||""===e)&&(e=document.getElementById("player")),(t===void 0||null===t||""===t)&&(t=document.getElementsByTagName("video")[0]),ee=void 0===l||null===l||""===l?"scripts/mkH5videos.json":l,(n===void 0||null===n||!0!==n)&&(n=!1);var le,ie,se,ne,de,oe=document.querySelector("meta[name=\"viewport\"]").content,re=!1,pe="http://www.w3.org/2000/svg";if(!t.controls){void 0===s||null===s||Object.getPrototypeOf(s)!==Object.prototype?(console.log("No settings."),s={topDir:"",videoOrder:!1,playlist:!1,uSetRez:"SD",rezColorUp:"#FD5600",rezColorSelect:"#458CCB",rezColorOver:"#ffffff",shoCtrlOnStart:"visible",buttonW:.1*H(e.style.height)+10+"px",buttonH:.1*H(e.style.height)+"px",btnColUp:"#458CCB",btnColOver:"#72ABDD",btnBgTop:"#040202",btnBgTopAlpha:1,btnBgBottom:"#10100A",btnBgBottomAlpha:1,nowPlayColor:"#91C7F6",bufferColor:"#FFFFFF",volOffset:10,hideCtrlsTime:3,progressFix:!1}):(console.log("settings"),void 0===s.topDir||"string"!=typeof s.topDir?(s.topDir="",O("Setting-topDir ( * Default * )")):O("Setting-topDir (Type expected, String:URL): "+s.topDir),void 0===s.videoOrder||"[object Array]"!==Object.prototype.toString.call(s.videoOrder)?(s.videoOrder=!1,O("Setting-videoOrder ( * Default * )")):O("Setting-videoOrder (Type expected, Array): "+s.videoOrder),void 0===s.playlist||"[object Array]"!==Object.prototype.toString.call(s.playlist)?(s.playlist=!1,O("Setting-playlist ( * Default * )")):O("Setting-playlist (Type expected, Array): "+s.playlist),void 0===s.uSetRez||"string"!=typeof s.uSetRez?(s.uSetRez="SD",O("Setting-uSetRez ( * Default * ):")):O("Setting-uSetRez (Default video resolution, XD | HD | HQ | SD | LQ | M): "+s.uSetRez),void 0===s.rezColorUp||"string"!=typeof s.rezColorUp?(s.rezColorUp="#FD5600",O("Setting-rezColorUp ( * Default * )")):O("Setting-rezColorUp (Type expected, Hexadecimal Color String): "+s.rezColorUp),void 0===s.rezColorSelect||"string"!=typeof s.rezColorSelect?(s.rezColorSelect="#458CCB",O("Setting-rezColorSelect ( * Default * )")):O("Setting-rezColorSelect (Type expected, Hexadecimal Color String): "+s.rezColorSelect),void 0===s.rezColorOver||"string"!=typeof s.rezColorOver?(s.rezColorOver="#ffffff",O("Setting-rezColorOver ( * Default * )")):O("Setting-rezColorOver (Type expected, Hexadecimal Color String): "+s.rezColorOver),void 0===s.shoCtrlOnStart||"string"!=typeof s.shoCtrlOnStart?(s.shoCtrlOnStart="visible",O("Setting-shoCtrlOnStart ( * Default * )")):O("Setting-shoCtrlOnStart (Type expected, hidden | visible): "+s.shoCtrlOnStart),void 0===s.buttonH||"string"!=typeof s.buttonH?(s.buttonH=.1*H(e.style.height)+"px",O("Setting-buttonH ( * Default * )")):O("Setting-buttonH (Type expected, String-number in pixels): "+s.buttonH),void 0===s.buttonW||"string"!=typeof s.buttonW?(s.buttonW=.1*H(e.style.height)+10+"px",O("Setting-buttonW ( * Default * )")):O("Setting-buttonW (Type expected, String-number in pixels): "+s.buttonW),void 0===s.btnColUp||"string"!=typeof s.btnColUp?(s.btnColUp="#458CCB",O("Setting-btnColUp ( * Default * )")):O("Setting-btnColUp (Type expected, Hexadecimal Color String): "+s.btnColUp),void 0===s.btnColOver||"string"!=typeof s.btnColOver?(s.btnColOver="#72ABDD",O("Setting-btnColOver ( * Default * )")):O("Setting-btnColOver (Type expected, Hexadecimal Color String): "+s.btnColOver),void 0===s.btnBgTop||"string"!=typeof s.btnBgTop?(s.btnBgTop="#040202",O("Setting-btnBgTop ( * Default * )")):O("Setting-btnBgTop (Type expected, Hexadecimal Color String): "+s.btnBgTop),void 0===s.btnBgTopAlpha||s.btnBgTopAlpha===s.btnBgTopAlpha&&s.btnBgTopAlpha!==(0|s.btnBgTopAlpha)?(s.btnBgTopAlpha=1,O("Setting-btnBgTopAlpha ( * Default * )")):O("Setting-btnBgTopAlpha (Type expected, Intager 0.0-1.0): "+s.btnBgTopAlpha),void 0===s.btnBgBottom||"string"!=typeof s.btnBgBottom?(s.btnBgBottom="#10100A",O("Setting-btnBgBottom ( * Default * )")):O("Setting-btnBgBottom (Type expected, Hexadecimal Color String): "+s.btnBgBottom),void 0===s.btnBgBottomAlpha||s.btnBgBottomAlpha===s.btnBgBottomAlpha&&s.btnBgBottomAlpha!==(0|s.btnBgBottomAlpha)?O("Setting-btnBgBottomAlpha ( * Default * )"):O("Setting-btnBgBottomAlpha (Type expected, Float 0.0-1.0): "+s.btnBgBottomAlpha),void 0===s.nowPlayColor||"string"!=typeof s.nowPlayColor?(s.nowPlayColor="#458CCB",O("Setting-nowPlayColor ( * Default * )")):O("Setting-nowPlayColor (Type expected, Hexadecimal Color String): "+s.nowPlayColor),void 0===s.bufferColor||"string"!=typeof s.bufferColor&&"[object Array]"!==Object.prototype.toString.call(s.bufferColor)?(s.bufferColor="#FFFFFF",O("Setting-bufferColor ( * Default * )")):O("Setting-bufferColor (Type expected, Hexadecimal Color String | Array | rainbow): "+s.bufferColor),void 0===s.volOffset||"number"!=typeof s.volOffset?(s.volOffset=0,O("Setting-volOffset ( * Default * )")):O("Setting-volOffset (Type expected, Number): "+s.volOffset),void 0===s.hideCtrlsTime||"number"!=typeof s.hideCtrlsTime?(s.hideCtrlsTime=3,O("Setting-hideCtrlsTime ( * Default * )")):O("Setting-hideCtrlsTime (Type expected, Number): "+s.hideCtrlsTime));var ye=document.createElement("div");e.appendChild(ye),ye.id="vidCtrl",ye.style.visibility=s.shoCtrlOnStart,ye.style.zIndex="50",ne=s.buttonH,de=s.buttonW;var ae=e.style.width,he=e.style.height,ue=e.style.top,ce=e.style.left;t.style.height=he,t.style.width=ae;var be=!1,me=5,ge=!1,ve=[],xe=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,fe=window.screen.height||document.documentElement.clientHeight||document.body.clientHeight,we=_(),Ce=window.getComputedStyle(document.body,null).getPropertyValue("font-family"),Ee=document.createElement("div");Ee.id="playBtn",Ee.style.width=de,Ee.style.height=ne,Ee.style.position="absolute",Ee.style.visibility="visible",Ee.style.zIndex="12",Ee.style.padding="5px",se=document.createElementNS(pe,"svg"),se.setAttribute("baseProfile","tiny"),se.setAttribute("xlmns",pe),se.setAttribute("viewBox","0 0 20 15"),se.setAttribute("preserveAspectRatio","none"),se.setAttribute("height",H(ne)),se.setAttribute("width",H(de)),se.setAttribute("x","0"),se.setAttribute("y","0"),se.style.display="block";var Se=document.createElementNS(pe,"g");Se.setAttribute("fill",s.btnBgBottom),se.appendChild(Se);var ze=document.createElementNS(pe,"path");ze.setAttribute("d","M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z"),Se.appendChild(ze);var Ae=document.createElementNS(pe,"path");Ae.setAttribute("d","M15.716.524c2.026 0 3.676 1.226 3.676 2.73v8.5c0 1.505-1.65 2.73-3.676 2.73H4.283c-2.026 0-3.675-1.226-3.675-2.73v-8.5c0-1.505 1.648-2.73 3.675-2.73h11.433m0-.304H4.283C2.037.22.2 1.587.2 3.257v8.498c0 1.67 1.837 3.034 4.083 3.034h11.433c2.246 0 4.084-1.37 4.084-3.04v-8.5c0-1.67-1.838-3.035-4.084-3.035z"),Se.appendChild(Ae);var Be=document.createElementNS(pe,"linearGradient");Be.setAttribute("id","a"),Be.setAttribute("gradientUnits","userSpaceOnUse"),Be.setAttribute("x1","9.999"),Be.setAttribute("y1","4.301"),Be.setAttribute("x2","9.999"),Be.setAttribute("y2","11.851");var Ie=document.createElementNS(pe,"stop");Ie.setAttribute("offset","0"),Ie.setAttribute("stop-color","#fff"),Be.appendChild(Ie);var Te=document.createElementNS(pe,"stop");Te.setAttribute("offset","1"),Be.appendChild(Te),se.appendChild(Be);var Le=document.createElementNS(pe,"path");Le.setAttribute("d","M15.716.828H4.282c-1.8 0-3.265 1.09-3.265 2.428v8.498c0 1.34 1.464 2.427 3.265 2.427h11.434c1.8 0 3.267-1.08 3.267-2.42v-8.5c0-1.34-1.466-2.426-3.267-2.426zm2.858 10.926c0 1.17-1.283 2.124-2.858 2.124H4.282c-1.575 0-2.857-.953-2.857-2.124V3.256c0-1.17 1.282-2.125 2.857-2.125h11.434c1.575 0 2.858.96 2.858 2.13v8.5z"),Le.setAttribute("fill","url(#a)"),se.appendChild(Le);var ke=document.createElementNS(pe,"path");ke.setAttribute("d","M15.716.828H4.283c-1.8 0-3.267 1.09-3.267 2.428V7.65c2.72.522 5.766.815 8.984.815s6.263-.293 8.983-.814V3.26c0-1.34-1.465-2.428-3.267-2.428z"),ke.setAttribute("fill",s.btnBgTop),se.appendChild(ke);var He=document.createElementNS(pe,"svg");He.setAttribute("xlmns",pe),He.setAttribute("viewBox","0 0 20 15"),He.setAttribute("preserveAspectRatio","none"),He.setAttribute("height",H(ne)),He.setAttribute("width",H(de)),He.setAttribute("x","0px"),He.setAttribute("y","0px"),He.style.position="absolute";var Oe=document.createElementNS(pe,"path");Oe.setAttribute("d","M6.212 3.974l7.913 3.834-7.913 3.834"),Oe.setAttribute("fill",s.btnColUp),Oe.setAttribute("id","play_btn");var _e=document.createElement("div");_e.id="playTop",_e.style.height=ne,_e.style.width=de,_e.style.position="absolute",_e.style.zIndex="15",_e.style.top="0px",_e.style.left="0px",_e.style.padding="5px",ve.push(Ee),Ee.appendChild(_e),He.appendChild(Oe),Ee.appendChild(He),Ee.appendChild(se),h(s.btnBgTopAlpha,s.btnBgBottomAlpha);var Ne=document.createElement("div");Ne.id="pauseBtn",Ne.style.width=de,Ne.style.height=ne,Ne.style.position="absolute",Ne.style.visibility="hidden",Ne.style.zIndex="10",Ne.style.padding="5px";var De=se.cloneNode(!0),Me=document.createElementNS(pe,"svg");Me.setAttribute("xlmns",pe),Me.setAttribute("viewBox","0 0 20 15"),Me.setAttribute("preserveAspectRatio","none"),Me.setAttribute("height",H(ne)),Me.setAttribute("width",H(de)),Me.setAttribute("x","0px"),Me.setAttribute("y","0px"),Me.style.position="absolute";var Fe=document.createElementNS(pe,"path");Fe.setAttribute("d","M6.212 4.312h2.56v6.993h-2.56zm5.12 0h2.56v6.993h-2.56z"),Fe.setAttribute("fill",s.btnColUp),Fe.setAttribute("id","pause_btn");var Re=document.createElement("div");Re.id="pauseTop",Re.style.height=ne,Re.style.width=de,Re.style.position="absolute",Re.style.zIndex="15",Re.style.top="0px",Re.style.left="0px",Re.style.padding="5px",ve.push(Ne),Ne.appendChild(Re),Me.appendChild(Fe),Ne.appendChild(Me),Ne.appendChild(De);var Ue=document.createElement("input");Ue.id="seeker",Ue.className="seekerC",Ue.type="range",Ue.min=0,Ue.max=100,Ue.value=1,Ue.step=.01,Ue.style.position="absolute",Ue.style.zIndex=12;var Pe=document.createElement("div");Pe.style.position="absolute",Pe.style.textAlign="right";var Ve=document.createElement("span");Ve.id="timeTxt",Ve.className="timeC",Ve.innerHTML="00:00 | ";var je=document.createElement("span");je.id="totalTxt",je.className="timeC",je.innerHTML="00:00";var Ge=document.createElement("div");Ge.id="muteBtn",Ge.style.width=de,Ge.style.height=ne,Ge.style.position="absolute",Ge.style.zIndex="10";var qe=se.cloneNode(!0),Xe=document.createElementNS(pe,"svg");Xe.setAttribute("xlmns",pe),Xe.setAttribute("viewBox","0 0 20 15"),Xe.setAttribute("preserveAspectRatio","none"),Xe.setAttribute("height",H(ne)),Xe.setAttribute("width",H(de)),Xe.setAttribute("x","0px"),Xe.setAttribute("y","0px"),Xe.style.position="absolute";var Ye=document.createElementNS(pe,"path");Ye.setAttribute("d","M5.042 6.416v2.662h2.28l.038.023 2.81-2.25V4.634L7.324 6.416H5.042zm8.675-.48l-.913.73c.177.32.28.688.28 1.08 0 .72-.344 1.37-.857 1.76l.646.836c.873-.554 1.45-1.51 1.45-2.596 0-.676-.226-1.302-.606-1.81zM10.17 10.86V8.78L8.71 9.946l1.457.913zm4.418-6.886l.37.297-9.193 7.374-.37-.297"),Ye.setAttribute("fill",s.btnColUp),Ye.setAttribute("id","mute_btn"),ve.push(Ge);var We=document.createElement("div");We.id="muteTop",We.style.height=ne,We.style.width=de,We.style.position="absolute",We.style.zIndex="15",Ge.appendChild(We),Xe.appendChild(Ye),Ge.appendChild(Xe),Ge.appendChild(qe);var Qe=document.createElement("div");Qe.id="unMuteBtn",Qe.style.width=de,Qe.style.height=ne,Qe.style.position="absolute",Qe.style.zIndex="12";var Ke=se.cloneNode(!0),Ze=document.createElementNS(pe,"svg");Ze.setAttribute("xlmns",pe),Ze.setAttribute("viewBox","0 0 20 15"),Ze.setAttribute("preserveAspectRatio","none"),Ze.setAttribute("height",H(ne)),Ze.setAttribute("width",H(de)),Ze.setAttribute("x","0px"),Ze.setAttribute("y","0px"),Ze.style.position="absolute";var Je=document.createElementNS(pe,"path");Je.setAttribute("d","M5.042 6.613V9.01H7.05l2.502 1.604V5.01L7.05 6.612m5.524-2.637l-.588.76c1.07.38 1.883 1.634 1.883 3.077 0 1.43-.8 2.676-1.858 3.066l.574.764c1.385-.58 2.37-2.075 2.37-3.83 0-1.762-.993-3.262-2.384-3.837z"),Je.setAttribute("fill",s.btnColUp),Je.setAttribute("id","unMute1_btn");var $e=document.createElementNS(pe,"path");$e.setAttribute("d","M11.43 5.453l-.576.744c.474.347.794.947.794 1.614 0 .65-.3 1.235-.753 1.587l.568.753c.768-.5 1.275-1.36 1.275-2.338 0-.992-.52-1.863-1.308-2.357z"),$e.setAttribute("fill",s.btnColUp),$e.setAttribute("id","unMute2_btn"),ve.push(Qe);var et=document.createElement("div");et.id="unMuteTop",et.style.height=ne,et.style.width=de,et.style.position="absolute",et.style.zIndex="15",Qe.appendChild(et),Ze.appendChild(Je),Ze.appendChild($e),Qe.appendChild(Ze),Qe.appendChild(Ke);var tt=document.createElement("input");tt.id="volumeSlider",tt.className="sliderC",tt.type="range",tt.min="0",tt.max="100",tt.value="75",tt.step="1",tt.style.position="absolute",tt.style.transform="rotate(270deg)",tt.style.width=.3*he-ne+"px";var lt=document.createElement("div");lt.id="rezBtn",lt.style.width=de,lt.style.height=ne,lt.style.position="absolute";var it=se.cloneNode(!0),st=document.createElementNS(pe,"svg");st.setAttribute("xlmns",pe),st.setAttribute("viewBox","0 0 20 15"),st.setAttribute("preserveAspectRatio","none"),st.setAttribute("height",H(ne)),st.setAttribute("width",H(de)),st.setAttribute("x","0px"),st.setAttribute("y","0px"),st.style.position="absolute";var nt=document.createElementNS(pe,"path");nt.setAttribute("d","M13.323 9.113c-.03 0-.06.003-.09.004l-3.09-2.39c-.216-.166-.53-.21-.804-.14l.48-1.093c.08-.18 0-.445-.18-.58l-1.01-.78c-.177-.135-.52-.197-.75-.136l-.608.16c-.1.026-.16.07-.178.124-.018.053.013.114.087.172l.65.5c.23.18.23.47 0 .647l-.694.54c-.11.09-.26.14-.417.14-.16 0-.31-.045-.42-.132l-.65-.5c-.075-.06-.153-.082-.22-.07-.07.013-.128.06-.16.138l-.21.47c-.08.18 0 .445.177.58l1.01.78c.175.135.518.197.75.136l1.42-.38c-.092.21-.034.455.183.62l3.09 2.39c0 .024-.003.047-.003.07 0 .698.73 1.265 1.634 1.265s1.637-.566 1.637-1.264c0-.697-.73-1.264-1.634-1.264zm0 1.927c-.473 0-.856-.297-.856-.662 0-.178.088-.344.25-.47.162-.124.377-.193.606-.193.472 0 .856.298.856.663s-.39.662-.86.662z"),nt.setAttribute("fill",s.btnColUp),nt.setAttribute("id","rez_btn"),ve.push(lt);var dt=document.createElement("div");dt.id="rezTop",dt.style.height=ne,dt.style.width=de,dt.style.position="absolute",dt.style.zIndex="15",lt.appendChild(dt),st.appendChild(nt),lt.appendChild(st),lt.appendChild(it);var ot=document.createElement("div");ot.id="fScreenBtn",ot.style.width=de,ot.style.height=ne,ot.style.position="absolute";var rt=se.cloneNode(!0),pt=document.createElementNS(pe,"svg");pt.setAttribute("xlmns",pe),pt.setAttribute("viewBox","0 0 20 15"),pt.setAttribute("preserveAspectRatio","none"),pt.setAttribute("height",H(ne)),pt.setAttribute("width",H(de)),pt.setAttribute("x","0px"),pt.setAttribute("y","0px"),pt.style.position="absolute";var yt=document.createElementNS(pe,"path");yt.setAttribute("d","M14.327 11.612h.63L14.95 4h-.63v.56h-.908v-.586H6.608v.004h-.02v.582h-.913v-.554h-.633l.006 7.61h.63v-.542h.915v.568h6.817v-.568h.914l.003.538zM7.54 10.576L7.538 5.04h4.923l.008 5.536H7.54zm-1.863-2.52h.915v1.03H5.68v-1.03zm.914-.48h-.91v-1.03h.915v1.03zm6.82-1.03h.916v1.03h-.913l-.002-1.03zm0 1.51h.917v1.03h-.914v-1.03zm.91-3.017l.005 1.03h-.915V5.04h.912zm-7.73 0v1.03h-.91V5.04h.913zm-.91 5.55V9.56h.916v1.03H5.68zm7.735 0v-.018l-.002-1.012h.916v1.03h-.914z"),yt.setAttribute("fill",s.btnColUp),yt.setAttribute("id","fScreen_btn"),ve.push(ot);var at=document.createElement("div");at.id="fScreenTop",at.style.height=ne,at.style.width=de,at.style.position="absolute",at.style.zIndex="15",ot.appendChild(at),pt.appendChild(yt),ot.appendChild(pt),ot.appendChild(rt);var ht=document.createElementNS(pe,"svg");ht.setAttribute("baseProfile","tiny"),ht.setAttribute("xlmns",pe),ht.setAttribute("viewBox","0 0 20 15"),ht.setAttribute("preserveAspectRatio","none"),ht.setAttribute("height",H(ne)),ht.setAttribute("width",H(de)),ht.setAttribute("x","0"),ht.setAttribute("y","0");var ut=document.createElementNS(pe,"ellipse");ut.setAttribute("cx","9.7908363"),ut.setAttribute("cy","7.3207169"),ut.setAttribute("rx","5.9063745"),ut.setAttribute("ry","5.9262953"),ut.setAttribute("fill",s.btnColOver),gsap.to(ut,{duration:.1,scaleX:0,scaleY:0,x:.1*H(ne),y:.1*H(de),overwrite:!0});let l=document.createElementNS(pe,"g");ht.appendChild(l),l.appendChild(ut);let i=document.createElementNS(pe,"path");i.setAttribute("d","m 11.281,8.145 v 2.633 c 0.125,0.072 0.377,0.23 0.377,0.49 0,0.418 -0.598,0.678 -1.415,0.678 -0.974,0 -2.294,-0.217 -2.294,-0.707 0,-0.173 0.251,-0.346 0.408,-0.403 V 8.158 C 6.77,8.102 5.235,7.968 3.766,7.774 c 0.291,3.312 2.933,5.902 6.151,5.902 3.214,0.004 5.852,-2.578 6.15,-5.883 -1.524,0.198 -3.135,0.306 -4.786,0.352 z"),i.setAttribute("fill",s.btnBgBottom),l.appendChild(i);let n=document.createElementNS(pe,"path");n.setAttribute("d","m 9.896,1.318 c -3.402,0 -6.16,2.759 -6.16,6.165 0,0.194 0.012,0.386 0.029,0.574 1.465,0.184 3.012,0.286 4.593,0.338 V 6.711 C 8.295,6.625 8.043,6.48 8.043,6.235 8.012,5.932 8.86,5.557 9.583,5.557 c 1.918,0 1.981,0.433 1.981,0.836 0,0.13 -0.094,0.289 -0.283,0.375 V 8.406 C 12.918,8.361 14.514,8.26 16.027,8.074 16.047,7.88 16.058,7.683 16.058,7.483 16.059,4.078 13.299,1.318 9.896,1.318 Z m -0.029,3.49 c -0.975,0 -1.886,-0.49 -1.886,-1.341 0,-0.418 0.503,-0.663 1.414,-0.663 1.195,0 1.886,0.577 1.886,1.312 0,0.403 -0.408,0.692 -1.414,0.692 z"),n.setAttribute("fill",s.btnBgTop),l.appendChild(n);let d=document.createElementNS(pe,"g");ht.appendChild(d);let o=document.createElementNS(pe,"path");o.setAttribute("d","M16.021,8.484c0,0-0.041,0.142-0.117,0.405    c-0.038,0.132-0.086,0.293-0.144,0.484c-0.038,0.188-0.138,0.415-0.278,0.674c-0.135,0.253-0.271,0.547-0.45,0.833    c-0.208,0.261-0.435,0.54-0.675,0.841c-0.267,0.275-0.597,0.521-0.92,0.804c-0.352,0.246-0.762,0.444-1.168,0.679    c-0.879,0.321-1.889,0.605-2.962,0.485c-0.28-0.032-0.509-0.021-0.84-0.096c-0.248-0.072-0.499-0.146-0.752-0.223    c-0.252-0.054-0.513-0.188-0.774-0.329c-0.255-0.145-0.539-0.26-0.77-0.438C5.214,11.93,4.461,10.96,3.96,9.86    C3.504,8.73,3.403,7.48,3.579,6.271c0.263-1.162,0.817-2.318,1.638-3.189C6.071,2.249,7.101,1.634,8.203,1.36    C9.3,1.15,10.43,1.118,11.398,1.479c0.492,0.102,0.93,0.38,1.357,0.603c0.428,0.231,0.748,0.58,1.104,0.847    c0.281,0.347,0.574,0.66,0.799,0.995c0.188,0.359,0.394,0.687,0.531,1.017c0.107,0.343,0.211,0.663,0.305,0.96    c0.059,0.304,0.074,0.591,0.107,0.846c0.099,0.51-0.027,0.915-0.024,1.188c-0.022,0.274-0.036,0.42-0.036,0.42L16.021,8.484z     M15.541,8.352c0,0,0.13-1.099,0.02-1.604c-0.041-0.252-0.063-0.536-0.129-0.834c-0.102-0.291-0.209-0.604-0.326-0.94    c-0.146-0.321-0.356-0.637-0.549-0.982c-0.229-0.322-0.523-0.621-0.805-0.951c-0.675-0.552-1.429-1.104-2.417-1.337    c-0.943-0.325-2.02-0.258-3.054-0.035C7.245,1.954,6.292,2.563,5.514,3.36C4.769,4.208,4.309,5.224,4.072,6.357    C3.944,7.477,4.066,8.613,4.504,9.628c0.484,0.983,1.184,1.836,2.05,2.413c0.208,0.152,0.443,0.23,0.658,0.352    c0.213,0.116,0.427,0.229,0.7,0.281c0.255,0.067,0.508,0.135,0.758,0.202c0.173,0.031,0.462,0.029,0.682,0.047    c0.946,0.086,1.815-0.196,2.571-0.487c0.347-0.217,0.697-0.395,0.995-0.612c0.271-0.256,0.553-0.472,0.773-0.715    c0.199-0.263,0.387-0.509,0.56-0.734c0.142-0.239,0.233-0.471,0.34-0.669c0.052-0.102,0.101-0.194,0.146-0.284    c0.039-0.1,0.061-0.211,0.089-0.305c0.051-0.192,0.094-0.356,0.129-0.489c0.067-0.267,0.106-0.407,0.106-0.407L15.541,8.352z"),o.setAttribute("fill",s.btnBgBottom),d.appendChild(o);var ct=document.createElement("div");ct.id="ctrls",ct.style.position="absolute",ct.style.zIndex="12",ct.style.padding=me+"px",ct.style.display="inline-block",ct.appendChild(Ee),ct.appendChild(Ne),ct.appendChild(Ue),Pe.appendChild(Ve),Pe.appendChild(je),ct.appendChild(Pe);var bt=document.createElement("div");bt.id="sideCtrl",bt.style.position="absolute",bt.style.display="block",bt.style.zIndex="12",bt.style.padding="5px 0px 5px 5px",bt.style.width=3*H(de)+"px",bt.style.height=ne,bt.style.margin="5px";var mt=document.createElement("div");mt.id="volCtrl",mt.style.position="absolute",mt.style.overflow="hidden",mt.style.zIndex="12",mt.style.display="block",mt.style.height=ne,mt.style.width=de;var gt=document.createElement("div");gt.id="sizeCtrl",gt.style.position="absolute",gt.style.zIndex="12",gt.style.display="block",gt.style.height=ne,gt.style.width=de,mt.appendChild(Ge),mt.appendChild(Qe),mt.appendChild(tt),bt.appendChild(mt),gt.appendChild(lt),bt.appendChild(gt),bt.appendChild(ot),ye.appendChild(ct),ye.appendChild(bt);var vt=document.createElement("div");vt.className="vidCtrlBG",vt.style.position="absolute",vt.style.width="100%",vt.style.zIndex="10",ye.appendChild(vt);var xt=document.createElement("div");for(let e in xt.style.overflow="hidden",ct.appendChild(xt),D(),screen.orientation.addEventListener("change",function(){console.log("Current orientation is "+screen.orientation.type)}),Ee.addEventListener("click",m,!1),Ne.addEventListener("click",m,!1),Ge.addEventListener("click",g,!1),Qe.addEventListener("click",g,!1),ot.addEventListener("click",v,!1),mt.addEventListener("mouseover",L,!1),mt.addEventListener("mouseout",k,!1),Ue.addEventListener("change",C,!1),tt.addEventListener("change",A,!1),t.addEventListener("timeupdate",B,!1),t.addEventListener("mousemove",function(){I(),clearTimeout(le),le=setTimeout(T,1e3*s.hideCtrlsTime)},!1),O("video.preload = "+t.preload),t.addEventListener("progress",E,!1),t.addEventListener("mouseover",function(){window.addEventListener("keydown",u,!1),window.addEventListener("keydown",c,!1),window.addEventListener("keydown",b,!1)},!1),t.addEventListener("mouseout",function(){t.removeEventListener("mousemove",I),window.removeEventListener("keydown",u,!1),window.removeEventListener("keydown",c,!1),window.removeEventListener("keydown",b,!1),T()},!1),ye.addEventListener("mouseover",function(){I(),clearTimeout(le)},!1),ye.addEventListener("mouseout",T,!1),ve){let t=ve[e];t.addEventListener("mouseover",y,!1),t.addEventListener("mouseout",a,!1)}var ft=gsap.fromTo(ye,{duration:.5,opacity:0,onComplete:function(){ye.style.visibility="hidden",Ee.style.visibility="hidden",Ne.style.visibility="hidden",Ge.style.visibility="hidden",Qe.style.visibility="hidden",t.style.cursor="none"}},{opacity:1,duration:.5,onComplete:function(){t.paused?(Ee.style.zIndex="12",Ee.style.visibility="visible",Ne.style.zIndex="10",Ne.style.visibility="hidden"):(Ee.style.zIndex="10",Ee.style.visibility="hidden",Ne.style.zIndex="12",Ne.style.visibility="visible"),t.muted?(Ge.style.zIndex="12",Ge.style.visibility="visible",Qe.style.zIndex="10",Qe.style.visibility="hidden"):(Ge.style.zIndex="10",Ge.style.visibility="hidden",Qe.style.zIndex="12",Qe.style.visibility="visible"),ye.style.visibility="visible",t.style.cursor="default"}});window.addEventListener("keydown",$,!1)}var wt,Ct=new XMLHttpRequest;ee.includes(".json")?Ct.open("GET",ee,!0):(wt=s.playlist?"functToGet=vidSort&playlist="+s.playlist.join(","):"functToGet=mkH5Videos",Ct.open("POST",ee,!0));var Et=null;Ct.setRequestHeader("Content-type","application/x-www-form-urlencoded"),Ct.onreadystatechange=function(){function l(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("_thumb","_pic"),i=document.getElementById(e.id.replace("_thumb","_title")),s=H(i.style.width),n=H(document.getElementById(e.id.replace("_thumb","_holder")).style.width);W(i,s,n);let d=document.getElementById(l);gsap.to(d,{duration:.1,scaleX:1.15,scaleY:1.15,overwrite:!0})}function i(t){let e=t.target||t.srcElement||window.event,l=e.id.replace("_thumb","_pic"),i=document.getElementById(l);gsap.to(i,{duration:.1,scaleX:1,scaleY:1,overwrite:!0})}function n(l){Ue.value=1,t.paused||(ge=!1,t.pause(),t.currentTime=0);let e=l.target||l.srcElement||window.event,i=e.id.replace("_thumb","");t.id=i,(null!==ie||ie!==void 0)&&(document.getElementById(K(ie)).style.color="#fff"),ie=i+"_title",document.getElementById(ie).style.color=s.nowPlayColor;let n=Et[i],o=Q();gt.removeChild(document.getElementById("rSizeRHolder")),document.getElementById("aboutBG").removeChild(document.getElementById("aboutScroller")),document.getElementById("aboutBG").removeChild(document.getElementById("aboutVidName"));let r=document.getElementById(e.id.replace("_thumb","_title")),p=H(document.getElementById(e.id.replace("_thumb","_title")).style.width),y=H(document.getElementById(e.id.replace("_thumb","_holder")).style.width);W(r,p,y),d(n),X(o,n),I(),clearTimeout(le)}function d(l){function n(t){let e=t.target||t.srcElement||window.event;e.style.color=s.rezColorUp,gsap.to(e,{duration:.1,scaleX:1.1,scaleY:1.1,overwrite:!0})}function d(t){let e=t.target||t.srcElement||window.event;gsap.to(e,{duration:.1,scaleX:1,scaleY:1,overwrite:!0}),e.style.color=e.id==s.uSetRez?s.rezColorSelect:s.rezColorOver}function o(l){xt.innerHTML="";let e=t.currentTime,i=l.target||l.srcElement||window.event,n=Q(),d=["XD","HD","HQ","SD","LQ","M"];if(null===document.getElementById(s.uSetRez)){for(let e in d.reverse())if(document.getElementById(null!==s.uSetRez)){document.getElementById(s.uSetRez).style.color=s.rezColorOver;break}}else document.getElementById(s.uSetRez).style.color=s.rezColorOver;s.uSetRez=i.id,i.style.color=s.rezColorUp,X(n,Et[t.id]),t.load(),t.currentTime=e,ge&&t.play()}function r(){gsap.to(nt,{duration:0,fill:s.btnColUp,scale:1,x:0,y:0,overwrite:!0}),gsap.to(nt,{duration:.5,fill:s.btnColOver,scale:1.2,x:-1,y:-1,overwrite:!0}),I?(B.play(),lt.style.zIndex="10",z.style.zIndex="12",S.style.zIndex="13",I=!1):p()}function p(){B.reverse(),z.style.zIndex="10",S.style.zIndex="10",lt.style.zIndex="12",I=!0}let y,h,g;if(void 0===l||""===l){h=1;let e=!1;for(g in Et){if(g==t.id){l=Et[g],e=!0;break}h++}e||O("If you want the resolution changer to work initially you must give your video a id that has a key from the json file.")}else for(g in h=1,Et){if(Et[g]==l)break;h++}let v=document.createElement("div");v.id="aboutVidTxt",v.style.display="block";let f=document.createElement("div");f.id="aboutHolder",f.style.position="relative",f.style.display="block";let w=document.createElement("div");w.id="aboutScroller",w.style.position="relative",w.style.display="block";let C=document.getElementById("aboutBG"),E=document.createElement("h3");E.id="aboutVidName",E.style.display="block",E.style.fontSize="14px",C.appendChild(E);let S=document.createElement("div");S.id="rSizeR",S.className="rSizeRC",S.style.position="absolute",S.style.width=de;let z=document.createElement("div");z.id="rSizeRHolder",z.style.position="absolute",z.style.width=de,z.style.overflow="hidden",z.appendChild(S),gt.appendChild(z),c=2,b=["XD","HD","HQ","SD","LQ","M"];let A=1;for(m in Et){if(A===h){for(y=0;y<=b.length-1;y++)for(u in Et[m].mp4)if(b[y]==u){let e=document.createElement("div");e.id=u,e.className="rezCtrls",e.style.width=de,e.style.height=ne,e.style.display="block",e.style.textAlign="center",0===y&&(e.style.padding="10px 0px 0px 0px"),e.style.color=e.id==s.uSetRez?s.rezColorSelect:s.rezColorOver,e.innerHTML=u,S.appendChild(e),e.style.bottom=c+"px",c+=H(ne)}V(Et[m].about,v),E.innerHTML=Et[m].name;break}A++}ot.style.zIndex="13",S.style.height=c+"px",S.style.left="0px",S.style.bottom="0px",S.style.zIndex="12",z.style.left="0px",z.style.bottom=H(ye.style.bottom)+H(ye.style.height)-me+"px",z.style.height="0px",z.style.zIndex="10";let B=gsap.fromTo(z,{duration:.5,height:c,ease:Power4.easeOut},{duration:.35,height:0,ease:Power2.easeIn});B.pause(B.endTime()),S.addEventListener("click",o,!1),S.addEventListener("mouseover",n,!1),S.addEventListener("mouseout",d,!1),f.appendChild(v),w.appendChild(f),C.appendChild(w),C.style.left=t.style.left,C.style.top=H(t.style.top)+H(t.style.height)+30+"px",C.style.width=void 0===a||null===a?H(e.style.width)+50+"px":H(e.style.width)+H(a.style.width)+"px",C.style.zIndex="11",w.style.width=void 0===a||null===a?H(C.style.width)+"px":H(e.style.width)+H(a.style.width)+"px",w.style.zIndex="11",w.style.margin="0px 10px 10px 0px",f.style.left="0px",f.style.top="0px",f.style.width=void 0===a||null===a?H(C.style.width)-15+"px":H(e.style.width)+H(a.style.width)-15+"px",f.style.zIndex="11",E.style.zIndex="2",v.style.zIndex="12",v.style.width="98%",v.style.margin="0 auto",lt.addEventListener("click",r,!1);let I=!1}if(4==Ct.readyState&&200==Ct.status){var o=Ct.responseText;Et=JSON.parse(o);var r=Q();if(2<Object.keys(Et).length){var p=document.createElement("div");p.id="playList",p.style.position="absolute";var y=document.createElement("div");y.id="playListScroll",y.style.overflow="auto",y.style.position="absolute";var a=document.createElement("div");a.id="pListHolder",a.style.position="absolute",y.appendChild(p),a.appendChild(y),e.appendChild(a);var h,u,c,b,m,g=0,x=0,f=0,w=Object.keys(Et).length,C=[];for(te=1;te<=w;te++)if(u=Object.keys(Et)[te],"settings"!=u){if(!s.videoOrder)h=Et[u];else if(void 0!==s.videoOrder[f]&&K(s.videoOrder[f])in Et&&-1===C.indexOf(K(s.videoOrder[f])))h=Et[K(s.videoOrder[f])],u=K(s.videoOrder[f]),C.push(K(s.videoOrder[f])),te--;else{if(void 0===Et[u])break;-1===C.indexOf(u)?(h=Et[u],C.push(u)):h=!1}if(!1!==h&&void 0!==h){let e=h.thumb[Object.keys(h.thumb)[0]];e.width>g&&(g=e.width);var E=document.createElement("div");E.id=u+"_holder",E.className="playListVideo_thumb",E.style.position="absolute",E.style.display="block",E.style.overflow="hidden",E.style.width=e.width+"px",E.style.height=e.height+"px",E.style.top=x+"px",p.appendChild(E);var S=document.createElement("img");if(S.id=u+"_pic",""===r)S.src=e.loc;else{S.src=r+"/"+e.loc}S.style.width=E.style.width,S.style.height=E.style.height,S.style.zIndex="13";var z=document.createElement("p");z.id=u+"_title",z.className="thumbTitle",z.style.position="absolute",z.style.bottom="0px",z.innerHTML=h.name,z.style.zIndex="15",z.style.overflow="visible",z.style.fontSize=12,z.style.width=Y(h.name,z.style.fontSize+"pt "+Ce)+"px";var A=document.createElement("div");A.id=u+"_nameHold",A.className="thumbTitleHolder",A.style.position="absolute",A.style.bottom="0px",A.style.left="0px",A.style.width=H(E.style.width)+"px",A.style.height=.36*e.height+"px",A.style.zIndex="14",A.style.backgroundColor="#000000",A.style.opacity=.7,A.appendChild(z);var B=document.createElement("div");B.id=u+"_thumb",B.style.position="absolute",B.style.top="0px",B.style.width=E.style.width,B.style.height=E.style.height,B.style.zIndex="16",E.appendChild(B),E.appendChild(S),E.appendChild(A);let d=H(window.getComputedStyle(z,null).getPropertyValue("width")),o=H(window.getComputedStyle(z,null).getPropertyValue("height")),y=H(A.style.width);z.style.right=N(y,d),z.style.width=d+2+"px",z.style.height=o+"px",document.getElementById(E.id).addEventListener("mouseover",l,!1),document.getElementById(E.id).addEventListener("mouseout",i,!1),document.getElementById(E.id).addEventListener("click",n,!1),0===x&&(ie=u+"_title",z.style.color=s.nowPlayColor,t.id=u,X(r,Et[u])),x=x+e.height+5,f++}}p.style.width=g+"px",p.style.zIndex="12",a.style.height=e.style.height,a.style.width=g+20+"px",a.style.zIndex="10",a.style.top="0px",a.style.left=H(t.style.left)+H(t.style.width)+10+"px",y.style.height=a.style.height,y.style.width=g+20+"px",y.style.opacity="1.0"}var T=document.createElement("div");T.id="aboutBG",T.style.position="absolute",T.style.display="block",T.className="aboutBG",e.appendChild(T),d()}},ee.includes(".json")?Ct.send(null):Ct.send(wt);let St=gsap.fromTo(mt,{duration:.5,height:H(ne),ease:Power4.easeOut},{duration:.5,height:.35*H(he),ease:Power2.easeIn});St.reverse()}"""

    #print(jsFile)
    if getJS is True:
        return jsFile
    
    jsPath = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.min.js')
    if not oExists(jsPath):
        jFile = open(jsPath, 'w')
        jFile.write(jsFile)
        jFile.close()
    return True


def mkPHP(baseDir):
    """  """
    phpFile = '''<?php
// $_POST['functToGet'] = 'vidSort';
// $_POST['playlist'] = 'KEEP,mkH5videos';
if(@$_POST['functToGet'] == 'vidSort'){
    // Use to sort by players
    if(@$_POST['playlist']){
        $playlist = explode(',', $_POST['playlist']);
    }
    $sortStrArr = array();
    foreach($playlist as $vid){
        $vid = (string)$vid;
        if($vid !== "" || !strlen($vid) > 150){
            array_push($sortStrArr, $vid);
        }
    }
    $jFile = file_get_contents('mkH5videos.json');
    $jData = json_decode($jFile, true);

    $newData = array();
    foreach ($jData as $key => $value){
        foreach($sortStrArr as $sortStr){
            if($key == $sortStr || $key == 'settings'){
                $newData[$key] = $value;
            }
        }
    }
    // print_r($newData);
    $jFile = json_encode($newData);
    echo $jFile;
}
if(@$_POST['functToGet'] == 'yTube'){
    $youtub_id = "VGazSZUYyf4";
    
    // https://www.youtube.com/watch?v=xP14lB42vFA

    //$images = json_decode(file_get_contents("https://gdata.youtube.com/feeds/api/videos/".$youtub_id."?v=2&alt=json"), true);
    $images = json_decode(file_get_contents("https://gdata.youtube.com/feeds/api/videos/?v=xP14lB42vFA&alt=json"), true);
    $images = $images['entry']['media$group']['media$thumbnail'];
    $image  = $images[count($images)-4]['url'];

    $maxurl = "https://i.ytimg.com/vi/".$youtub_id."/maxresdefault.jpg";
    $max    = get_headers($maxurl);

    if (substr($max[0], 9, 3) !== '404') {
        $image = $maxurl;   
    }

    echo '<img src="'.$image.'">';
}
if(@$_POST['functToGet'] == 'mkH5Videos'){
    // Gets all the videos; this is the same as just loading the JSON directly. //
    $jFile = file_get_contents('mkH5videos.json');
    // $jData = json_decode($jFile, true);
    // $jFile = json_encode($jData);

    echo $jFile;
}
else{
    die();
}
?>'''
    #print(phpFile)
    phpPath = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.php')
    pFile = open(phpPath, 'w')
    pFile.write(phpFile)
    pFile.close()


def mkCSS(baseDir):
    """ This function will create the CSS file. """
    cssFile = R'''


/*------------------- Important notes ------------------*/
/* This file was generated by the mkH5videos.py program.
The program will do its best to take most of the styling
off your hands. !important the colors for the range inputs
in this file come from the mkH5videos.json settings object.
The thumbnail and track for the volume and time scrubber 
will be set to the main button colors by default. 

** I have given most of the elements created by javaScript
an id or a class name. If you need to style something that
isn't in this css firebug is->was your friend. ** */

/* The background div for the thumbnails */
#pListHolder{
    position: relative;
    left: 900px;
    height: 900px;
    background: #333;
    padding: 10px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.90;
}

/* The video selector thumbnail scroller */
#playListScroll{
    height: 900px;
}

/* The video selector thumbnail text */
.thumbTitle{
    color: #fff;
    text-align: right;
    /*text-indent: 10px;*/
    font-size: 10px;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
    margin: 0px;
    
}

/* The background div for the about video text */
#aboutBG{
    width: 600px;
    background: #333;
    padding: 10px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.90;
    z-index: -1;
    padding: 10px;
}

/* the about video paragraphs */
#aboutVidTxt p{
    color: #fff;
    text-indent: 15px;
    font-size: 12px;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
}

/* the about video paragraphs */
#aboutVidTxt_FS p{
    color: #fff;
    text-indent: 25px;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
}

/* the about video header */
#aboutBG h3{
    color: **thumbColor**;
    text-indent: 10px;
    /*font-size: 14px;*/
    font-weight: bold;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
}

/* The about holder scroller. Uncomment to allow scrolling
 of fixed height content. Comment out to allow dynamically
 changing height of content! */
#aboutScroller{
    position: absolute;
    height: 150px;
    overflow: auto;
}

/* the background for the video controls */
.vidCtrlBG{
    background: #333;
    padding: 0px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.75;
}

/* the background for the resolution controls */
.rSizeRC{
    background: #333;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px 10px 0px 0px;
    opacity: 0.75;
}

/* the class for the resolution control buttons */
.rezCtrls{
    font-size: 14px;
    font-family: 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';
    font-weight: bolder;
    font-style: italic;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
}

/* The div that holds the video player and controls*/
#player{
    background: #000;
}

/*-------- The button controls. --------*/
/*-------- Needs CSS Mobile Love --------*/
#playBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#playBtn:hover{
    opacity: 1;
}

#pauseBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#pauseBtn:hover{
    opacity: 1;
}

#muteBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#muteBtn:hover{
    opacity: 1;
}

#unMuteBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#unMuteBtn:hover{
    opacity: 1;
}

#rezBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#rezBtn:hover{
    opacity: 1.0;
}

#fScreenBtn{
    border: none;
    cursor: pointer;
    opacity: 0.72;
}

#fScreenBtn:hover{
    opacity: 1;
}

/* the time tracker text ( 0:24:19 | 1:25:37 )*/

.timeC{
    font-size: 11px;
    font-family: 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';
    color: #fff;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
}

/* enable this if you want the background to not have 
white dots in the corners. only an issue in webkit but
looks funny in fireFox */
input[type='range']{
    background-color: rgba(0, 0, 0, 0);
    border: 0px;
    opacity: 0.8;
}

/*------------------- Volume Range Input ------------------*/

/* This is where you may need to find the volume thumbnail
and background track colors. I will make a comment above them
so you can find them easily.  */

/* Needs Testing */
.sliderC::input[type='range']{
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
    transform: rotate(270deg);
}

.sliderC {
    -webkit-appearance: none;
    width: 100%;
    margin: 13px 0;
    cursor:  pointer;
}
.sliderC:focus {
    outline: none;
}


/* Chrome, Opera */
.sliderC::-webkit-slider-runnable-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the volume 1 */
    background: **trackColor**;
    border-radius: 4.9px;
    border: 1.8px solid #010101;
}

.sliderC::-webkit-slider-thumb {
    position: relative;
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 18px;
    width: 10px;
    border-radius: 9px;
    top: 2.5px;
    /* the thumbnail for the volume 1 */
    background: **thumbColor**;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -7.45px;
}

.sliderC:focus::-webkit-slider-runnable-track {
    background: #949393;
}


/* Fire Fox */
.sliderC::-moz-range-track {
    width: 100%;
    height: 3.7px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the volume 2 */
    background: **trackColor**;
    border-radius: 4.9px;
    border: 1.8px solid #010101;
}

.sliderC::-moz-range-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 15px;
    width: 10px;
    border-radius: 9px;
    /* the thumbnail for the volume 2 */
    background: **thumbColor**;
    cursor: pointer;
}

/* Satan */
.sliderC::-ms-track {
    width: 100%;
    height: 3.7px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
}

.sliderC::-ms-fill-lower {
    background: #7a7979;
    border: 1.8px solid #010101;
    border-radius: 9.8px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}

.sliderC::-ms-fill-upper {
    /* The track color for the volume 3 */
    background: **trackColor**;
    border: 1.8px solid #010101;
    border-radius: 9.8px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}

.sliderC::-ms-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 15px;
    width: 10px;
    border-radius: 9px;
    /* the thumbnail for the volume 3 */
    background: **thumbColor**;
    cursor: pointer;
    height: 3.7px;
}

.sliderC:focus::-ms-fill-lower {
    background: **trackColor**;
}

.sliderC:focus::-ms-fill-upper {
    /* The track color for the volume 4 */
    background: #949393;
}

/*_________________ End Volume Range Input ________________*/

/*--------------------Time Range Input---------------------*/

/* This is where you may need to find the video time thumbnail
and background track colors. I will make a comment above them
so you can find them easily.  */

/* Chrome, Opera */
.seekerC {
    -webkit-appearance: none;
    width: 150px;
    margin: 0.05px 0;
}

.seekerC:focus {
    outline: none;
}

.seekerC::-webkit-slider-runnable-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the video 1 */
    background: **trackColor**;
    border-radius: 4.7px;
    border: 2.2px solid #010101;
}

.seekerC::-webkit-slider-thumb {
    position: relative;
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    border-radius: 9px;
    top: 1px;
    /* the thumbnail for the video 1 */
    background: **thumbColor**;

    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -2.25px;
}

.seekerC:focus::-webkit-slider-runnable-track {
    background: #949393;
}


/* Fire Fox */
.seekerC::-moz-range-track {
    width: 100%;
    height: 9.9px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the video 2 */
    background: **trackColor**;
    background: rgba(0, 0 ,0 ,0);
    border-radius: 4.7px;
    border: 2.2px solid #010101;
}

.seekerC::-moz-range-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    border-radius: 9px;
    /*background-image: :url(#vThumb);*/
    /* the thumbnail for the video 2 */
    background: **thumbColor**;
    cursor: pointer;
}

.seekerC::-moz-range-progress {
    /* the done track color for the video 1 */
    background-color: **trackDoneColor**;
    height: 9.9px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    border-radius: 4.7px;
    border: 2.2px solid #010101; 
}

/* Satan */
.seekerC::-ms-track {
    width: 100%;
    height: 9.9px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
}

.seekerC::-ms-fill-lower {
    /* the done track color for the video 2 */
    background: **trackDoneColor**;
    border: 2.2px solid #010101;
    border-radius: 9.4px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}

.seekerC::-ms-fill-upper {
    /* The track color for the video 3 */
    background: **trackColor**;
    border: 2.2px solid #010101;
    border-radius: 9.4px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}

.seekerC::-ms-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    border-radius: 9px;
    /* the thumbnail for the video 3 */
    background: **thumbColor**;
    cursor: pointer;
    height: 9.9px;
}

.seekerC:focus::-ms-fill-lower {
    /* the done track color for the video 3 */
    background: **trackColor**;
}

.seekerC:focus::-ms-fill-upper {
    /* The track color for the video 4 */
    background: #949393;
}

/*__________________ End Time Range Input _________________*/


'''
    jsonFile = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
    # print(jsonFile)
    if oExists(jsonFile):
        jFexists = True
        try:
            jsData = open(jsonFile)
            jData = json.load(jsData)
            jsData.close()
        except ValueError:
            jData = json.loads(jsonFile)
    else:
        jFexists = False

    if jFexists:
        cssFile = cssFile.replace('**thumbColor**', jData['settings']['thumbColor'])
        cssFile = cssFile.replace('**trackColor**', jData['settings']['trackColor'])
        cssFile = cssFile.replace('**trackDoneColor**', jData['settings']['trackColorDone'])
        cssPath = oJoin(baseDir, 'mkH5videos', 'scripts', 'mkH5videos.css')
        cFile = open(cssPath, 'w')
        cFile.write(cssFile)
        cFile.close()
        print(cssFile)
    else:
        print('To make the css file the json file must exist.')


class MutableDict(dict):
    """ Class that extends python3.7 dictionary to include insert_before and 
    insert_after methods. """

    def insert_before(self, key, newKey, val):
        """ Insert newKey:value into dict before key"""
        try:
            __keys = list(self.keys())
            __vals = list(self.values())

            insertAt = __keys.index(key)

            __keys.insert(insertAt, newKey)
            __vals.insert(insertAt, val)

            self.clear()
            self.update({x: __vals[i] for i, x in enumerate(__keys)})
            
        except ValueError as e:
            raise e

    def insert_after(self, key, newKey, val):
        """ Insert newKey:value into dict after key"""
        try:
            __keys = list(self.keys())
            __vals = list(self.values())

            insertAt = __keys.index(key) + 1

            if __keys[-1] != key:
                __keys.insert(insertAt, newKey)
                __vals.insert(insertAt, val)
                self.clear()
                self.update({x: __vals[i] for i, x in enumerate(__keys)})
            else:
                self.update({newKey: val})

        except ValueError as e:
            raise e


if __name__ == '__main__':
    args = [x for x in argv[1:] if '=' not in x]
    krgs = {x.split('=', 1)[0]: x.split('=', 1)[1] for x in argv[1:] if '=' in x}
    mkH5Video(*args, **krgs)
